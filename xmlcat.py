#!/usr/bin/env python

import sys
import xml.dom.minidom as dom

def concatenate(target, sources):
	name = None
	nodes = []

	for path in sources:
		document = dom.parse(path)
		element = document.documentElement

		if element.hasAttributes():
			print >>sys.stderr, '%s: Warning: Document element has attributes' % path

		if name is None:
			name = element.tagName
		elif name != element.tagName:
			print >>sys.stderr, '%s: Warning: Document element name differs from others' % path

		for node in element.childNodes:
			clone = node.cloneNode(deep=True)
			nodes.append(clone)

	document = dom.getDOMImplementation().createDocument(None, name, None)
	element = document.documentElement

	for node in nodes:
		element.appendChild(node)

	if target:
		file = open(target, 'w')
	else:
		file = sys.stdout

	document.writexml(file, encoding='UTF-8')

	if target:
		file.close()

if __name__ == '__main__':
	concatenate(None, sys.argv[1:])
