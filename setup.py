#!/usr/bin/env python

import xmlcat

# Options

import sys
import getopt

options = {
	'help': False,
	'prefix': None,
	'destdir': None,
	'no-compile': False,
	'no-gconf': False,
}

def usage(file):
	print >>file, 'Usage:', sys.argv[0], 'install [--help] [--prefix=PATH] [--destdir=PATH] [--no-gconf] [--no-compile]'

try:
	optspec = []

	for name in options:
		if type(options[name]) is bool:
			opt = name
		else:
			opt = '%s=' % name
		optspec.append(opt)

	args = sys.argv[1:]
	install = False

	for arg in args:
		if arg == 'install':
			args.remove(arg)
			install = True
			break

	if not install:
		usage(sys.stderr)
		sys.exit(1)

	opts, args = getopt.getopt(args, '', optspec)

	if args:
		usage(sys.stderr)
		sys.exit(1)

	for opt, value in opts:
		name = opt.strip('-')
		if name in options:
			if type(options[name]) is bool:
				options[name] = True
			else:
				options[name] = value
		else:
			usage(sys.stderr)
			sys.exit(1)

	if options['help']:
		usage(sys.stdout)
		sys.exit(0)

except getopt.GetoptError:
	usage(sys.stderr)
	sys.exit(1)

# Utilities

import os
from os.path import join, split, dirname, exists, abspath
from glob import glob
import shutil
import compileall

def get_option(name, default=None):
	value = options[name]
	if value is not None:
		return value
	else:
		return default

def install(target, source):
	print 'Installing', target

	make_leading_dirs(target)
	shutil.copyfile(source, target)
	os.chmod(target, 0644)

def make_leading_dirs(path):
	directory = dirname(path)
	if not exists(directory):
		os.makedirs(directory)

# Configured paths

prefix = get_option('prefix', '/usr/local')

bin_prefix = join(prefix, 'bin')
share_prefix = join(prefix, 'share')

app_modules_prefix = join(share_prefix, 'encode')
app_data_prefix = join(share_prefix, 'encode')

locale_prefix = join(share_prefix, 'locale')
desktop_prefix = join(share_prefix, 'applications')
schemas_prefix = join(share_prefix, 'gconf', 'schemas')

# Installation paths

destdir = get_option('destdir', '')

bin_destdir = destdir + abspath(bin_prefix)

app_modules_destdir = destdir + abspath(app_modules_prefix)
app_data_destdir = destdir + abspath(app_data_prefix)

locale_destdir = destdir + abspath(locale_prefix)
desktop_destdir = destdir + abspath(desktop_prefix)
schemas_destdir = destdir + abspath(schemas_prefix)

# Version

file = open('ChangeLog')
entry = file.readline()
file.close()

components = entry.split()
version = components[1]

# Install script

path = join(bin_destdir, 'encode')
print 'Installing', path

file = open('encode.py.in')
source = file.read()
file.close()

source = source.replace('@LOCALEDIR@', locale_prefix)
source = source.replace('@MODULEDIR@', app_modules_prefix)
source = source.replace('@DATADIR@', app_data_prefix)
source = source.replace('@VERSION@', version)

make_leading_dirs(path)
file = open(path, 'w')
file.write(source)
file.close()
os.chmod(path, 0755)

# Install other files

for path in glob(join('encode', '*.py')):
	install(join(app_modules_destdir, path), path)

for path in glob(join('encode', '*.glade')) + glob(join('encode', '*.ui')):
	install(join(app_data_destdir, path), path)

install(join(desktop_destdir, 'encode.desktop'), 'encode.desktop')

schemas_path = join(schemas_destdir, 'encode.schemas')
make_leading_dirs(schemas_path)
xmlcat.concatenate(schemas_path, glob(join('encode', '*.schemas')))

# Compile Python modules

if not get_option('no-compile'):
	compileall.compile_dir(join(app_modules_destdir, 'encode'))

# Register GConf schemas

if not get_option('no-gconf'):
	args = ['gconftool-2', '--makefile-install-rule', schemas_path]
	os.environ['GCONF_CONFIG_SOURCE'] = ''
	os.spawnvp(os.P_WAIT, args[0], args)
