Encode 0.5.3 (2006-08-03)

  * Extension API documentation

Encode 0.5.2 (2006-08-01)

  * Output fix

Encode 0.5.1 (2006-08-01)

  * encode.options module made accessible

Encode 0.5 (2006-08-01)

  * Totally different extension system based on GObjects
  * Lots of internal changes for no good reason
  * Diffviewer extension interface renamed to differ
  * Drag-and-drop and reorder (some) tabs
  * Modules installed under /usr/share by default

  Mozilla:
  * Removed

  Project:
  * Options extension interface
  * GnomeVFS is used instead of Gamin
  * Destination row existence is checked when drag-and-dropping
  * Handler items are sorted

  Options:
  * GuessOptions extension guesses the best action
  * PythonOptions extension supports options written in Python

Encode 0.4 (2006-02-16)

  * Huge changes to the extension APIs and Encode internals...
  * UIManager-based API for adding menus.
  * All Environment actions are in a dedicated Environment menu.
  * Quit and delete confirmation dialogs removed.
  * Tab books configurable via GConf.
  * Start script passes the optimize option to the interpreter (which is
    hardcoded to /usr/bin/python for now).
  * View class naming scheme changed: GConf variable backward
    compatibility is broken.

  Project:
  * Uses separate UI description for menus.
  * Separate Actions menu removed.
  * New Handler extension API for special file format viewers.  Accessible
    via the file popup menu.

  Output:
  * Abort item's sensitivity is consistent (closes: #1360215).

  Vim:
  * Removed crappy Diff-viewer implementation.

  Mozilla:
  * MozEmbed-based HTML viewer.
  * Implements the Handler API.

  Emacs:
  * Experimental Editor implementation.  Requires a pre-release Emacs with
    unofficial patches.

Encode 0.3.1 (2005-12-03)

  * Separate changelogs for the application and the Debian package.
  * Build separate package for Ubuntu.

Encode 0.3 (2005-12-02)

  * Save window size and maximization state (closes: #1360733).
  * Save horizontal and vertical paned positions (closes: #1360729).
  * View areas can shrink.
  * Diff Viewer interface introduced.
  * Extension class constructor interface changed.

  Preferences:
  * Extensible settings infrastructure.
  * The Preferences window is shown at first startup.
  * Configure menu item is replaced with Preferences; gconf-editor is no
    longer used.

  Project:
  * Controller interface introduced.  Controller extensions are scanned
    to find one that supports the version control system used by a given
    directory tree.  Default controller implements file and directory
    manipulation functions without a version control system back-end.
  * Drag-and-drop support: files and directories can be moved within the
    project tree and copied to and from external applications.
  * Files and directories can be registered (Control) and unregistered
    (Remove) with the version control system.
  * Directories are drawn with a shaded background color.
  * The Run action in the popup menu is only available for executables.

  Darcs:
  * Controller implementation for the darcs revision control system
    (closes: #1361123).

  Vim:
  * Implement the Diff Viewer interface.
  * Program and default mode settings.

  Terminal:
  * Drag-and-drop support: text, URLs and filenames can be copied into
    the terminal.
  * Font and scrollback lines settings.

  Output:
  * Font setting.

  Xephyr:
  * Enable scrolling if the X server is too large.

Encode 0.2 (2005-11-23)

  * TODO items moved to the Task Tracking System at SourceForge.
  * SouceForge's Bug Tracking System taken into use.

  * Open associated file when clicking output message (closes: #1361152).
  * Output messages are parsed using configurable regular expressions.
  * Extension architecture changed to be more flexible.
  * An installation script is provided in the source package.
  * Some API documentation written.

Encode 0.1.1 (2005-11-12)

  * Require PyGTK 2.6 instead of 2.8
  * Recommend GConf editor
  * Website moved to SourceForge
  * License changed to LGPL

Encode 0.1 (2005-11-03)

  * Initial release.

