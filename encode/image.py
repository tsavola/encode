# Copyright 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import os
import gobject
import gtk
import fcntl
from gettext import gettext

import view
import environment
import execution

##
## Channel

def get_channel(size):
	view = get_view(size)
	return Channel(view)

class Channel (execution.Channel):
	def __init__(self, view):
		execution.Channel.__init__(self, gobject.IO_IN, view.read)
		self.connect('closed', self.on_closed, view)

	def open(self, fd):
		fcntl.fcntl(fd, fcntl.F_SETFL, os.O_NONBLOCK)
		file = os.fdopen(fd, 'r')
		Channel.open(self, file)

	def on_closed(self, channel, view):
		view.stop()

gobject.type_register(Channel)

##
## View

views = []

def get_views():
	global views
	return [v for v in views if not v.is_busy()]

def get_view():
	global views

	view = None
	for v in views:
		if not v.is_busy():
			view = v
			break

	if view:
		environment.show_view(view, focus=False)
	else:
		view = View()
		environment.add_view(view, 'top')

	return view

class View (view.ViewExtension):
	def __init__(self):
		view.ViewExtension.__init__(self)

		text = gettext('Image')
		self.label = gtk.Label(text)
		self.label.show()

		self.scrolled = gtk.ScrolledWindow()
		self.scrolled.connect('parent-set', self.on_scrolled_parent_set)
		self.scrolled.connect('destroy', self.on_scrolled_destroy)
		self.scrolled.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		self.scrolled.set_shadow_type(gtk.SHADOW_NONE)
		self.scrolled.show()

		viewport = gtk.Viewport()
		viewport.set_shadow_type(gtk.SHADOW_NONE)
		viewport.show()
		self.scrolled.add(viewport)

		self.image = gtk.Image()
		self.image.show()
		viewport.add(self.image)

		self.pixmap = None
		self.size = None
		self.offset = None
		self.invalid = gtk.gdk.Rectangle()

	def get_label(self):
		return self.label

	def get_widget(self):
		return self.scrolled

	def on_scrolled_parent_set(self, scrolled, old_parent):
		new_parent = scrolled.get_parent()
		if new_parent and not old_parent:
			global views
			views.append(self)

	def on_scrolled_destroy(self, scrolled):
		global views
		views.remove(self)

	def is_busy(self):
		return self.offset is not None

	def start(self, size):
		width, height = size

		self.pixmap = gtk.gdk.Pixmap(None, width, height, 24)
		self.image.set_from_pixmap(self.pixmap, None)

		self.size = size
		self.offset = 0, 0

		self.invalid.x = 0
		self.invalid.width = width

		self.image.realize()
		gc = self.image.style.bg_gc[gtk.STATE_NORMAL]
		self.pixmap.draw_rectangle(gc, True, 0, 0, width, height)
		self.image.queue_draw()

	def stop(self):
		self.offset = None

		self.image.queue_draw()

	def read(self, file, flush):
		while True:
			data = file.read()
			if data:
				self.draw(data)

			if not flush or not data:
				break

	def draw(self, data):
		gc = self.image.style.fg_gc[gtk.STATE_NORMAL]

		data_offset = 0
		data_length = len(data) / 4  # bold assumption

		width, height = self.size
		x, y = self.offset

		orig_y = y

		while data_length > 0:
			buffer = data[data_offset:]
			length = min(width - x, data_length)

			self.pixmap.draw_rgb_32_image(gc, x, y, length, 1, gtk.gdk.RGB_DITHER_NONE, buffer, 0)

			data_offset += length * 4
			data_length -= length

			x += length
			if x == width:
				x = 0
				y += 1
				if y == height:
					y = 0

		if y > orig_y:
			self.invalid.y = orig_y
			self.invalid.height = y - orig_y

			if x > 0:
				self.invalid.height += 1

			self.image.window.invalidate_rect(self.invalid, False)

		elif y < orig_y:
			self.invalid.y = 0
			self.invalid.height = height

			self.image.window.invalidate_rect(self.invalid, False)

		self.offset = x, y

gobject.type_register(View)
