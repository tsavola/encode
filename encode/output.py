# Copyright 2005, 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

# TODO:
#   - Abort function (popup menu)
#   - Allow copying of output text to clipboard
#   - Filters
#
# BUGS:
#   - Cell height problems
#   - Output line wrapping

import os
import re
import gobject
import pango
import gtk
from gettext import gettext
from cStringIO import StringIO

import view
import scroll
import execution
import config
import environment
import settings

datadir = None

def init_extension(d):
	global datadir
	datadir = d

views = []

def get_views():
	global views
	return [i for i in views if not i.is_busy()]

bloated = os.environ['HOME'] + os.sep
compact = '~' + os.sep

def pretty_args(args):
	str = ''
	for i in args:
		if str:
			str = '%s %s' % (str, i)
		else:
			str = i
	return str

def pretty_vars(vars):
	str = None
	if vars:
		for i in vars:
			s = '%s=%s' % (i, vars[i])
			if str is None:
				str = s
			else:
				str = '%s %s' % (str, s)
	return str

def pretty_workdir(path):
	if not path:
		path = os.getcwd()

	if path.startswith(bloated):
		return path.replace(bloated, compact, 1)
	else:
		return path

def get_line_height(context, font_desc):
	language = context.get_language()
	metrics = context.get_metrics(font_desc, language)
	points = metrics.get_ascent() + metrics.get_descent()
	return pango.PIXELS(points)

def render_background(window, widget, background_area, expose_area, flags):
	if not (flags & gtk.CELL_RENDERER_SELECTED):
		x, y, w, h = background_area
		widget.style.paint_flat_box(window, gtk.STATE_NORMAL, gtk.SHADOW_NONE,
		                            expose_area, widget, None, x, y, w, h)

##
## Job rendering

class JobData (gobject.GObject):
	def __init__(self, job):
		gobject.GObject.__init__(self)
		self.workdir_path = job.workdir
		self.workdir = pretty_workdir(job.workdir)
		self.vars = pretty_vars(job.vars)
		self.args = pretty_args(job.args)

	def edit(self, name, line):
		path = os.path.join(self.workdir_path, name)
		environment.edit(path, self.workdir_path, line)

gobject.type_register(JobData)

class JobRenderer:
	def __init__(self):
		self.layout_normal = None
		self.layout_shaded = None
		self.line_height = None

	def build_layout(self, widget, shaded=False, inverted=False):
		context = widget.get_pango_context()
		layout = pango.Layout(context)

		if shaded:
			if inverted:
				c = widget.style.white
			else:
				c = widget.style.bg[gtk.STATE_SELECTED]

			attrs = pango.AttrList()
			attrs.insert(pango.AttrForeground(c.red, c.green, c.blue, 0, -1))

			layout.set_attributes(attrs)

		return layout

	def get_layouts(self, widget, inverted):
		if self.layout_normal is None:
			self.layout_normal = self.build_layout(widget)

			self.layout_shaded = (
				self.build_layout(widget, True, False),
				self.build_layout(widget, True, True),
			)

		assert type(inverted) is bool
		index = int(inverted)

		return self.layout_normal, self.layout_shaded[index]

	def get_size(self, data, widget, cell_area):
		if self.line_height is None:
			context = widget.get_pango_context()
			self.line_height = get_line_height(context, widget.style.font_desc)

		if data.vars:
			lines = 3
		else:
			lines = 2

		height = self.line_height * lines

		return 0, 0, 0, height

	def render(self, data, window, widget, background_area, cell_area, expose_area, flags):
		assert self.line_height is not None

		render_background(window, widget, background_area, expose_area, flags)

		contents = [
			(gettext('Directory:'), data.workdir),
			(gettext('Variables:'), data.vars),
			(gettext('Command:'),   data.args),
		]

		selected = bool(flags & gtk.CELL_RENDERER_SELECTED)
		value_layout, key_layout = self.get_layouts(widget, inverted=selected)

		x = cell_area[0] + 4
		y = cell_area[1]

		key_width = 0

		for key, value in contents:
			key_layout.set_text(key)

			width, height = key_layout.get_pixel_size()
			key_width = max(key_width, width)

			if value is None:
				continue

			widget.style.paint_layout(window, gtk.STATE_NORMAL, True, expose_area,
			                          widget, None, x, y, key_layout)

			y += self.line_height

		x = cell_area[0] + 4 + key_width + 4
		y = cell_area[1]

		for key, value in contents:
			if value is None:
				continue

			value_layout.set_text(value)

			widget.style.paint_layout(window, gtk.STATE_NORMAL, True, expose_area,
			                          widget, None, x, y, value_layout)

			y += self.line_height

##
## Output rendering

class OutputData (gobject.GObject):
	def __init__(self, text):
		gobject.GObject.__init__(self)
		self.text = text

class StdoutData (OutputData):
	def __init__(self, text):
		OutputData.__init__(self, text)

gobject.type_register(StdoutData)

class StderrData (OutputData):
	def __init__(self, text):
		OutputData.__init__(self, text)

gobject.type_register(StderrData)

class StdoutRenderer:
	def __init__(self, font_desc):
		self.font_desc = font_desc
		self.layout = None

		self.attrs = pango.AttrList()
		self.attrs.insert(pango.AttrFontDesc(font_desc, 0, -1))

	def get_layout(self, widget):
		if self.layout is None:
			context = widget.get_pango_context()
			self.layout = pango.Layout(context)
			self.layout.set_attributes(self.attrs)

#			language = context.get_language()
#			metrics = context.get_metrics(self.font_desc, language)
#			char_width = metrics.get_approximate_char_width()
#
#			self.layout.set_indent(-8 * char_width)
#			self.layout.set_wrap(pango.WRAP_CHAR)

		return self.layout

	def get_size(self, data, widget, cell_area):
#		cell_width = cell_area[2]
#
#		layout = self.get_layout(widget)
#		layout.set_width(cell_width * pango.SCALE)
#		layout.set_text(data.text)
#		width, height = layout.get_pixel_size()

		width = 0

		context = widget.get_pango_context()
		height = get_line_height(context, self.font_desc)

		return 0, 0, width, height

	def render(self, data, window, widget, background_area, cell_area, expose_area, flags):
		layout = self.get_layout(widget)
		layout.set_text(data.text)

		x = cell_area[0]
		y = cell_area[1]

		widget.style.paint_layout(window, gtk.STATE_NORMAL, True, expose_area,
		                          widget, None, x, y, layout)

class StderrRenderer (StdoutRenderer):
	def __init__(self, font_desc):
		StdoutRenderer.__init__(self, font_desc)
		self.attrs.insert(pango.AttrForeground(0xa000, 0, 0, 0, -1))

##
## Status rendering

class SuccessData (gobject.GObject):
	def __init__(self):
		gobject.GObject.__init__(self)

gobject.type_register(SuccessData)

class ErrorData (gobject.GObject):
	def __init__(self, value):
		gobject.GObject.__init__(self)
		self.value = value

gobject.type_register(ErrorData)

class SignalData (gobject.GObject):
	def __init__(self, value):
		gobject.GObject.__init__(self)
		self.value = value

gobject.type_register(SignalData)

class StatusRenderer:
	def __init__(self):
		self.layout = None
		self.size = None
		self.attrs = pango.AttrList()

	def get_layout(self, widget):
		if self.layout is None:
			context = widget.get_pango_context()
			self.layout = pango.Layout(context)
			self.layout.set_attributes(self.attrs)

		return self.layout

	def get_size(self, data, widget, cell_area):
		if self.size is None:
			context = widget.get_pango_context()
			height = get_line_height(context, widget.style.font_desc)
			self.size = 0, 0, 0, height

		return self.size

	def render(self, data, window, widget, background_area, cell_area, expose_area, flags):
		render_background(window, widget, background_area, expose_area, flags)

		text = self.get_text(data)
		layout = self.get_layout(widget)
		layout.set_text(text)

		x = cell_area[0] + 4
		y = cell_area[1]

		widget.style.paint_layout(window, gtk.STATE_NORMAL, True, expose_area, widget, None, x, y, layout)

class SuccessRenderer (StatusRenderer):
	text = gettext('Success')

	def __init__(self):
		StatusRenderer.__init__(self)
		self.attrs.insert(pango.AttrForeground(0, 0x9000, 0, 0, -1))

	def get_text(self, data):
		return self.text

class FailureRenderer (StatusRenderer):
	def __init__(self):
		StatusRenderer.__init__(self)
		self.attrs.insert(pango.AttrForeground(0xa000, 0, 0, 0, -1))

	def get_text(self, data):
		return self.format % data.value

class ErrorRenderer (FailureRenderer):
	format = gettext('Terminated with error code: %d')

	def __init__(self):
		FailureRenderer.__init__(self)

class SignalRenderer (FailureRenderer):
	format = gettext('Terminated by signal: %d')

	def __init__(self):
		FailureRenderer.__init__(self)

##
## Separator

class Separator (gobject.GObject):
	def __init__(self):
		gobject.GObject.__init__(self)

gobject.type_register(Separator)

Separator.instance = Separator()

class SeparatorRenderer:
	def get_size(self, separator, widget, cell_area):
		return 0, 0, 0, 0

	def render(self, separator, window, widget, background_area, cell_area, expose_area, flags):
		pass

def is_separator_row(store, iter):
	data = store.get_value(iter, 0)
	return isinstance(data, Separator)

##
## CellRenderer

class CellRenderer (gtk.GenericCellRenderer):
	__gproperties__ = {
		'data': (gobject.TYPE_OBJECT, None, None, gobject.PARAM_WRITABLE),
	}

	def __init__(self, font_desc):
		gtk.GenericCellRenderer.__init__(self)
		self.set_property('xpad', 1)
		self.set_property('ypad', 0)

		self.slaves = {
			JobData:     JobRenderer(),
			StdoutData:  StdoutRenderer(font_desc),
			StderrData:  StderrRenderer(font_desc),
			SuccessData: SuccessRenderer(),
			ErrorData:   ErrorRenderer(),
			SignalData:  SignalRenderer(),
			Separator:   SeparatorRenderer(),
		}

		self.current_slave = None
		self.current_data = None

	def do_set_property(self, property, value):
		if property.name == 'data':
			self.current_slave = self.slaves[type(value)]
			self.current_data = value
		else:
			gtk.GenericCellRenderer.do_set_property(self, property, value)

	def on_get_size(self, widget, cell_area):
		return self.current_slave.get_size(self.current_data, widget, cell_area)

	def on_render(self, window, widget, background_area, cell_area, expose_area, flags):
		self.current_slave.render(self.current_data, window, widget, background_area, cell_area, expose_area, flags)

gobject.type_register(CellRenderer)

##
## Output

class Output (environment.OutputExtension):
	def __init__(self):
		environment.OutputExtension.__init__(self)

	def get_view(self):
		views = get_views()
		if views:
			return views[0]
		else:
			view = View()
			environment.add_view(view, 'bottom')
			return view

	def set_batch(self, batch):
		view = self.get_view()
		view.set_batch(batch)
		environment.show_view(view, focus=False)

gobject.type_register(Output)

##
## Launcher

class Launcher (environment.LauncherExtension):
	def __init__(self):
		environment.LauncherExtension.__init__(self)

		label = gettext('Output window')
		self.action = gtk.Action(None, label, None, None)
		self.action.set_visible(True)

	def get_action(self):
		return self.action

	def create(self):
		return View()

gobject.type_register(Launcher)

##
## View

class View (view.ViewExtension):
	no_attrs = pango.AttrList()

	filters = (
		(re.compile('[[:alnum:]/\._-]*cc.+-c.+[[:space:]]([[:alpha:]/]+)\.c'), ''),
	)

	def __init__(self):
		view.ViewExtension.__init__(self)

		text = gettext('Output')
		self.label = gtk.Label(text)
		self.label.show()

		self.conf = config.Directory('encode/output')

		self.formats = []
		self.conf.add_strings('formats', self.set_formats)

		font = self.conf.get_string('font')
		font_desc = pango.FontDescription(font)

		self.store = gtk.TreeStore(gobject.TYPE_OBJECT)

		render = CellRenderer(font_desc)
		column = gtk.TreeViewColumn(None, render, data=0)
#		column.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
#		column.set_fixed_width(500)

		self.treeview = gtk.TreeView(self.store)
		self.treeview.connect('row-activated', self.on_treeview_row_activated)
		self.treeview.set_headers_visible(False)
		self.treeview.set_enable_search(False)
		self.treeview.set_reorderable(False)
		self.treeview.set_row_separator_func(is_separator_row)
		self.treeview.append_column(column)
		self.treeview.show()

		self.scroller = scroll.VScroller(self.treeview)
		self.scroller.connect('parent-set', self.on_scroller_parent_set)
		self.scroller.connect('destroy', self.on_scroller_destroy)
		self.scroller.show()

		self.busy_attrs = pango.AttrList()
		self.busy_attrs.insert(pango.AttrWeight(pango.WEIGHT_BOLD, 0, -1))
		self.busy_attrs.insert(pango.AttrStyle(pango.STYLE_ITALIC, 0, -1))

		self.busy_cursor = gtk.gdk.Cursor(gtk.gdk.WATCH)

		self.batch = None
		self.handlers = None

		self.current_iter = None
		self.current_path = None

		self.stdout_buffer = StringIO()
		self.stderr_buffer = StringIO()

	def set_formats(self, strings):
		self.formats = [re.compile(s) for s in strings]

	def set_batch(self, batch):
		if self.batch is not None:
			for id in self.handlers:
				self.batch.disconnect(id)

		self.batch = batch

		self.handlers = (
			self.batch.connect('job-started', self.on_batch_job_started),
			self.batch.connect('finished', self.on_batch_finished),
		)

		self.batch.get_output().consumers.add(self.on_batch_stdout)
		self.batch.get_error().consumers.add(self.on_batch_stderr)

		self.store.clear()

	def get_label(self):
		return self.label

	def get_widget(self):
		return self.scroller

	def is_busy(self):
		return self.batch and self.batch.is_alive()

	def on_scroller_parent_set(self, scroller, old_parent):
		new_parent = scroller.get_parent()
		if new_parent and not old_parent:
			global views
			views.append(self)

	def on_scroller_destroy(self, scroller):
		global views
		views.remove(self)

		self.conf.remove_all()
		self.conf = None

		if self.batch:
			for id in self.handlers:
				self.batch.disconnect(id)

			self.batch.stop()

			self.batch = None
			self.handlers = None

	def on_batch_job_started(self, batch, job):
		if self.current_iter is not None:
			self.label.set_attributes(self.busy_attrs)
			self.treeview.window.set_cursor(self.busy_cursor)

			self.store.append(None, [Separator.instance])

		data = JobData(job)
		iter = self.store.append(None, [data])

		self.current_iter = iter
		self.current_path = self.store.get_path(iter)
		self.treeview.scroll_to_cell(self.current_path)

	def on_batch_finished(self, batch, status):
		self.label.set_attributes(self.no_attrs)
		self.treeview.window.set_cursor(None)

		self.store.append(None, [Separator.instance])

		if status == 0:
			data = SuccessData()
		else:
			retval = status >> 8
			signum = status & 255

			if signum != 0:
				data = SignalData(signum)
			else:
				data = ErrorData(retval)

		iter = self.store.append(None, [data])
		path = self.store.get_path(iter)
		self.treeview.scroll_to_cell(path)

		self.current_iter = None
		self.current_path = None

	def on_batch_stdout(self, text):
		self.process_data(text, self.stdout_buffer, StdoutData)

	def on_batch_stderr(self, text):
		self.process_data(text, self.stderr_buffer, StderrData)

	def process_data(self, text, buffer, datatype):
		iter = None

		offset = buffer.tell()
		buffer.seek(0, 2)
		buffer.write(text)
		buffer.seek(offset)

		while True:
			line = buffer.readline()
			size = len(line)

			if size == 0:
				buffer.truncate(0)
				buffer.seek(0)
				break

			if line[size-1] != '\n':
				buffer.seek(-size, 1)
				break

			line = line[:size-1]

			data = datatype(line)
			iter = self.store.append(self.current_iter, [data])

		if iter is not None:
			self.treeview.expand_row(self.current_path, False)

			path = self.store.get_path(iter)
			self.treeview.scroll_to_cell(path)

	def on_treeview_row_activated(self, treeview, path, column):
		iter = self.store.get_iter(path)
		data = self.store.get_value(iter, 0)

		if isinstance(data, OutputData):
			name = None
			line = None

			for regex in self.formats:
				match = regex.match(data.text)
				if match is not None:
					name, line_str = match.group(1, 2)
					line = int(line_str)
					break

			if name is not None:
				job_iter = self.store.iter_parent(iter)
				job_data = self.store.get_value(job_iter, 0)

				job_data.edit(name, line)

gobject.type_register(View)

##
## Settings

if 'set' not in __builtins__:
	from sets import Set as set

class Settings (settings.SettingsExtension):
	def __init__(self):
		settings.SettingsExtension.__init__(self)

		path = os.path.join(datadir, 'encode', 'output-settings.glade')
		self.glade = gtk.glade.XML(path)
		self.glade.signal_autoconnect(self)

		self.conf = config.Directory('encode/output')

		self.dirty = set((
			'font',
		))
		self.revert()

	def get_name(self):
		return gettext('Output')

	def get_widget(self):
		return self.glade.get_widget('table')

	def apply(self):
		if 'font' in self.dirty:
			fontbutton = self.glade.get_widget('font_fontbutton')
			font = fontbutton.get_font_name()

			self.conf.set_string('font', font)

		self.dirty.clear()

	def revert(self):
		if 'font' in self.dirty:
			font = self.conf.get_string('font')

			fontbutton = self.glade.get_widget('font_fontbutton')
			fontbutton.set_font_name(font)

		self.dirty.clear()

	def on_font_set(self, fontbutton):
		self.dirty.add('font')
		self.modified()

gobject.type_register(Settings)
