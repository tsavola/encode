# Copyright 2005, 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import os
import gobject
import pango
import gtk
import gtk.glade
from gettext import gettext

if 'set' not in __builtins__:
	from sets import Set as set

import settings
import extension

##
## Preferences

class Preferences (gobject.GObject):
	current = (None, None)

	def __init__(self, datadir):
		gobject.GObject.__init__(self)

		path = os.path.join(datadir, 'encode', 'preferences.glade')
		self.glade = gtk.glade.XML(path)
		self.glade.signal_autoconnect(self)

		# Tree

		treeview = self.glade.get_widget('treeview')

		store = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_OBJECT, gobject.TYPE_INT, gobject.TYPE_BOOLEAN)
		treeview.set_model(store)

		render = gtk.CellRendererText()
		render.set_property('foreground', 'red')
		column = gtk.TreeViewColumn(None, render)
		column.add_attribute(render, 'text', 0)
		column.add_attribute(render, 'foreground-set', 3)
		treeview.append_column(column)

		selection = treeview.get_selection()
		selection.connect('changed', self.on_selection_changed)

		# Extensions

		extensions = []

		for klass in extension.get_children(settings.SettingsExtension):
			ext = klass()
			pri = ext.get_priority()
			name = ext.get_name()

			extensions.append((-pri, name, ext))

		extensions.sort()

		# Pages

		for npri, name, ext in extensions:
			self.add_page(store, ext)

		selection.select_iter(store.get_iter_root())

		# Dirty

		self.dirty = set()

		# Show

		window = self.glade.get_widget('window')
		window.show()

	def add_page(self, store, settings):
		widget = settings.get_widget()

		align = gtk.Alignment(xscale=1, yscale=1)
		align.set_padding(12, 12, 12, 12)
		align.show()
		align.add(widget)

		notebook = self.glade.get_widget('notebook')
		page = notebook.append_page(align)

		name = settings.get_name()
		iter = store.append((name, settings, page, False))

		settings.connect('modified', self.on_settings_modified, iter)

	def on_selection_changed(self, selection):
		self.current = selection.get_selected()
		store, iter = self.current

		page = store.get_value(iter, 2)
		notebook = self.glade.get_widget('notebook')
		notebook.set_current_page(page)

		self._update_buttons()

	def on_settings_modified(self, settings, iter):
		if settings.accept:
			store = self.current[0]
			store.set_value(iter, 3, True)

			self._update_buttons()

			self.dirty.add(settings)

	def on_apply_clicked(self, button):
		store, iter = self.current

		settings = store.get_value(iter, 1)
		try:
			settings.accept = False
			settings.apply()
		finally:
			settings.accept = True

		store.set_value(iter, 3, False)

		self._update_buttons()

		self.dirty.remove(settings)

	def on_revert_clicked(self, button):
		store, iter = self.current

		settings = store.get_value(iter, 1)
		try:
			settings.accept = False
			settings.revert()
		finally:
			settings.accept = True

		store.set_value(iter, 3, False)

		self._update_buttons()

		self.dirty.remove(settings)

	def on_close_clicked(self, button):
		close = self._check_unapplied()
		if close:
			window = self.glade.get_widget('window')
			window.destroy()

	def on_window_delete(self, window, event):
		close = self._check_unapplied()
		return not close

	def on_window_destroy(self, window):
		self.emit('quit')

	def _check_unapplied(self):
		if self.dirty:
			window = self.glade.get_widget('window')

			dialog = gtk.MessageDialog(
				window, gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
				gtk.MESSAGE_QUESTION, gtk.BUTTONS_NONE,
				gettext('There are unsaved changes'))
			dialog.add_button(gtk.STOCK_APPLY,  gtk.RESPONSE_APPLY)
			dialog.add_button(gtk.STOCK_CLOSE,  gtk.RESPONSE_CLOSE)
			dialog.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
			dialog.set_default_response(gtk.RESPONSE_APPLY)

			response = dialog.run()
			dialog.destroy()

			if response == gtk.RESPONSE_APPLY:
				for settings in self.dirty:
					settings.apply()

				self.dirty.clear()

			return response != gtk.RESPONSE_CANCEL
		else:
			return True

	def _update_buttons(self):
		store, iter = self.current
		modified = store.get_value(iter, 3)

		for name in ('apply_button', 'revert_button'):
			widget = self.glade.get_widget(name)
			widget.set_sensitive(modified)

gobject.type_register(Preferences)
gobject.signal_new('quit', Preferences, 0, gobject.TYPE_NONE, [])

##
## Main

def main(datadir):
	try:
		extension.load(None, 'encode.*', datadir)

		def on_quit(prefs):
			gtk.main_quit()

		prefs = Preferences(datadir)
		prefs.connect('quit', on_quit)

		gtk.main()

	except KeyboardInterrupt:
		pass
