# Copyright 2005, 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import os
import gobject
import gtk
import gtk.glade
from gettext import gettext

import environment
import view
import execution
import config
import extension
import settings

datadir = None
conf = config.Directory('encode/emacs')
views = []

def init_extension(d):
	global datadir
	datadir = d

##
## Handler

class Handler (environment.HandlerExtension):
	command = 'emacsclient'

	def __init__(self):
		environment.HandlerExtension.__init__(self)

		conf.add_string('client_command', self.set_command)

		label = gettext('Open in Emacs')
		self.action = gtk.Action(None, label, None, None)
		self.action.set_visible(True)

		self.executor = execution.Executor()

	def set_command(self, command):
		self.command = command

	def get_action(self):
		return self.action

	def get_view(self):
		global views
		if views:
			return views[0]
		else:
			raise environment.ViewNotFound, 'Emacs not running'

	def eval(self, view, expr):
		server = view.get_server()
		args = [self.command, '--socket-name', server, '--eval', expr]

		self.executor.start(execution.Job(args))

	def supports_file(self, path):
		global views
		return bool(views)

	def handle_file(self, path):
		self.open_file(path)

	def open_file(self, path, workdir=None, line=None):
		path_expr = string_expr(path)

		if line is not None:
			expr = '(let* ((f %s)' \
			       '       (b (find-buffer-visiting f)))' \
			       ' (if b (switch-to-buffer b)' \
			       '       (find-file f))' \
			       ' (goto-line %d))' \
			       % (path_expr, line)
		else:
			expr = '(let* ((f %s)' \
			       '       (b (find-buffer-visiting f)))' \
			       ' (if b (switch-to-buffer b)' \
			       '       (find-file f)))' \
			       % path_expr

		view = self.get_view()
		self.eval(view, expr)

		environment.show_view(view, focus=True)

	def close_file(self, path):
		expr = '(let ((b (find-buffer-visiting %s)))' \
		       ' (if b (kill-buffer b)))' \
		       % Handler.string_expr(path)

		global views
		for view in views:
			self.eval(view, expr)

	def cleanup(self):
		global views
		for view in views:
			self.eval(view, '(save-buffers-kill-emacs)')

gobject.type_register(Handler)

def string_expr(path):
	return '"%s"' % path.replace('"', '\\"')

##
## Launcher

class Launcher (environment.LauncherExtension):
	def __init__(self):
		environment.LauncherExtension.__init__(self)

		label = gettext('Emacs text editor')
		self.action = gtk.Action(None, label, None, None)
		self.action.set_visible(True)

	def get_action(self):
		return self.action

	def create(self):
		return View()

gobject.type_register(Launcher)

##
## View

class View (view.Embedder):
	command = 'emacs'

	def __init__(self):
		view.Embedder.__init__(self)

		conf.add_string('command', self.set_command)

		text = gettext('Emacs')
		self.label = gtk.Label(text)
		self.label.show()

		self.executor = execution.Executor()
		self.server = None

	def set_command(self, command):
		self.command = command

	def is_alive(self):
		return self.executor.is_alive()

	def get_server(self):
		return self.server

	def client_start(self, xid):
		self.server = 'encode-%d' % xid

		args = [self.command, '--no-splash', '--parent-id', str(xid),
		        '--eval', '(setq server-name "%s")' % self.server,
		        '--eval', '(server-start)',
		        '--eval', '(setq confirm-kill-emacs nil)']

		self.executor.start(execution.Job(args))

		global views
		views.append(self)

	def client_destroy(self):
		global views
		views.remove(self)

		self.server = None
		self.executor.stop()

	def get_label(self):
		return self.label

gobject.type_register(View)

# Settings

if 'set' not in __builtins__:
	from sets import Set as set

class Settings (settings.SettingsExtension):
	def __init__(self):
		settings.SettingsExtension.__init__(self)

		path = os.path.join(datadir, 'encode', 'emacs-settings.glade')
		self.glade = gtk.glade.XML(path)
		self.glade.signal_autoconnect(self)

		self.conf = config.Directory('encode/emacs')

		self._init('command')
		self._init('client_command')

		self.dirty = set()

	def _init(self, key):
		path = self.conf.get_string(key)

		if not os.path.isabs(path):
			fullpath = find_binary(path)
			if fullpath:
				path = fullpath
				self.conf.set_string(key, path)

		filechooser = self.glade.get_widget('%s_filechooser' % key)
		filechooser.set_filename(path)

	def get_name(self):
		return gettext('Emacs')

	def get_widget(self):
		return self.glade.get_widget('root')

	def apply(self):
		for key in self.dirty:
			filechooser = self.glade.get_widget('%s_filechooser' % key)
			path = filechooser.get_filename()
			self.conf.set_string(key, path)

		self.dirty.clear()

	def revert(self):
		for key in self.dirty:
			path = self.conf.get_string(key)
			filechooser = self.glade.get_widget('%s_filechooser' % key)
			filechooser.set_filename(path)

		self.dirty.clear()

	def on_command_changed(self, filechooser):
		new_path = filechooser.get_filename()
		old_path = self.conf.get_string('command')

		if new_path and new_path != old_path:
			self.dirty.add('command')
			self.modified()

	def on_client_command_changed(self, filechooser):
		new_path = filechooser.get_filename()
		old_path = self.conf.get_string('client_command')

		if new_path and new_path != old_path:
			self.dirty.add('client_command')
			self.modified()

gobject.type_register(Settings)

def find_binary(name):
	for dir in os.environ['PATH'].split(':'):
		path = os.path.join(dir, name)
		if os.path.exists(path):
			return path
	return None
