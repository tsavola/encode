# Copyright 2005, 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import sys
import os
import signal
import glob
import time
import gobject
import gtk
import gtk.glade
from gettext import gettext

if 'set' not in __builtins__:
	from sets import Set as set

import extension
import config
import execution
import tab
import settings
import view

datadir = None

def init_extension(d):
	global datadir
	datadir = d

class Environment (gobject.GObject):
	def __init__(self, version):
		gobject.GObject.__init__(self)

		self.connect('quit', self.on_quit)

		# Config

		self.conf = config.Directory('encode')

		# Glade

		path = os.path.join(datadir, 'encode', 'environment-main.glade')
		self.glade = gtk.glade.XML(path)
		self.glade.signal_autoconnect(self)

		window = self.glade.get_widget('window')

		# UI manager

		ui = get_ui_manager()

		group = ui.get_accel_group()
		window.add_accel_group(group)

		# UI actions

		group = gtk.ActionGroup('encode-environment')

		action = gtk.Action('environment', gettext('Environment'), None, None)
		group.add_action(action)

		action = gtk.Action('about', None, None, gtk.STOCK_ABOUT)
		action.connect_after('activate', self.on_about_activate)
		group.add_action(action)

		action = gtk.Action('preferences', None, None, gtk.STOCK_PREFERENCES)
		action.connect_after('activate', self.on_preferences_activate)
		group.add_action(action)

		action = gtk.Action('quit', None, None, gtk.STOCK_QUIT)
		action.connect_after('activate', self.on_quit_activate)
		group.add_action(action)

		action = gtk.Action('create-view', gettext('_New'), None, gtk.STOCK_NEW)
		group.add_action(action)

		action = gtk.Action('select-view', gettext('_Select'), None, gtk.STOCK_GO_FORWARD)
		group.add_action(action)

		action = gtk.Action('delete-view', gettext('_Delete'), None, gtk.STOCK_DELETE)
		action.connect_after('activate', self.on_view_delete_activate)
		group.add_action(action)

		ui.insert_action_group(group, -1)

		# UI description

		path = os.path.join(datadir, 'encode', 'environment.ui')
		ui.add_ui_from_file(path)

		box = self.glade.get_widget('left_box')
		menubar = ui.get_widget('/menubar')
		box.pack_start(menubar, expand=False, fill=True)

		# Resource tracking

		self.book_locations = []
		self.books_by_view = {}
		self.views_by_widget = {}

		self.target_book = None
		self.target_view = None

		self.launchers = []

		# Books

		group = gtk.AccelGroup()
		window.add_accel_group(group)

		left_box = self.glade.get_widget('left_box')
		right_paned = self.glade.get_widget('right_paned')

		book = self._create_book('tabs_left')
		left_box.pack_start(book, expand=True, fill=True)
		self.book_locations.append((book, 'left'))

		book = self._create_book('tabs_top', ['vertical'], group, '<Ctrl>1')
		right_paned.pack1(book, resize=True, shrink=True)
		self.book_locations.append((book, 'top'))

		book = self._create_book('tabs_bottom', ['vertical'], group, '<Ctrl>2')
		right_paned.pack2(book, resize=True, shrink=True)
		self.book_locations.append((book, 'bottom'))

		# Extensions

		for ext in self.conf.get_strings('extensions', []):
			tuple = ext.split(':')

			if len(tuple) == 1:
				name, = tuple
				extension.load(None, name, datadir)

			elif len(tuple) == 2:
				path, name = tuple
				extension.load(path, name, path)

			else:
				path, name, data = tuple
				extension.load(path, name, data)

		# Launchers

		actions_by_label = []

		for type in extension.get_children(LauncherExtension):
			launcher = extension.get_singleton(type)
			self.launchers.append(launcher)

			action = launcher.get_action()
			action.connect_after('activate', self.on_view_create_activate, launcher)

			label = action.get_property('label')
			actions_by_label.append((label, action))

		item = ui.get_widget('/book-popup/create-view')
		menu = item.get_submenu()

		actions_by_label.sort()

		for label, action in actions_by_label:
			item = action.create_menu_item()
			menu.append(item)

		# Defaults

		self.default_handler = None
		self.default_differ = None
		self.default_output = None

		self.conf.add_string('default_handler', self.set_default_handler)
		self.conf.add_string('default_differ', self.set_default_differ)
		self.conf.add_string('default_output', self.set_default_output)

		# Version

		dialog = self.glade.get_widget('about_dialog')
		dialog.set_property('version', version)

		# Window

		width, height = self.conf.get_ints('window_geometry', (-1, -1))
		maximize = self.conf.get_bool('window_maximize', True)

		window.set_default_size(width, height)
		if maximize:
			window.maximize()

		center_position = self.conf.get_int('paned_position_center', -1)
		center_paned = self.glade.get_widget('center_paned')
		center_paned.set_position(center_position)

		right_position = self.conf.get_int('paned_position_right', -1)
		right_paned = self.glade.get_widget('right_paned')
		right_paned.set_position(right_position)

		window.show()

		# Initial views

		views = self.conf.get_strings('views', [])

		for entry in views:
			classname, location = entry.split('=', 1)

			cls = extension.get_class(classname)
			view = cls()

			self.add_view(view, location=location)

		for book, location in self.book_locations:
			book.first_view()

	def get_book(self, location):
		for book, loc in self.book_locations:
			if location == loc:
				return book

		raise EnvironmentError, 'Location does not exist: %s' % location

	# Window

	def on_window_configure(self, window, event):
		geometry = (event.width, event.height)
		self.conf.set_ints('window_geometry', geometry)

		return False

	def on_window_state(self, window, event):
		maximize = event.new_window_state & gtk.gdk.WINDOW_STATE_MAXIMIZED
		self.conf.set_bool('window_maximize', maximize)

		return False

	def on_center_paned_notify(self, paned, param):
		position = paned.get_position()
		self.conf.set_int('paned_position_center', position)

	def on_right_paned_notify(self, paned, param):
		position = paned.get_position()
		self.conf.set_int('paned_position_right', position)

	# Defaults

	def set_default_handler(self, classname):
		cls = extension.get_class(classname)
		obj = extension.get_singleton(cls)
		self.default_handler = obj

	def set_default_differ(self, classname):
		cls = extension.get_class(classname)
		obj = extension.get_singleton(cls)
		self.default_differ = obj

	def set_default_output(self, classname):
		cls = extension.get_class(classname)
		obj = extension.get_singleton(cls)
		self.default_output = obj

	# Books

	def _create_book(self, name, defaults=[], accel_group=None, accelerator=None):
		keywords = self.conf.get_strings(name, defaults)

		tabs_first = 'first' in keywords

		if 'horizontal' in keywords:
			book = tab.HTabBook(tabs_first)
			book.connect('tab-right-button-press', self.on_book_right)
		elif 'vertical' in keywords:
			book = tab.VTabBook(tabs_first)
			book.connect('tab-right-button-press', self.on_book_right)
		else:
			book = tab.TablessBook()

		if accelerator:
			key, mod = gtk.accelerator_parse(accelerator)
			book.add_accelerator('next-view', accel_group, key, mod, 0)

		book.show()

		return book

	def on_book_right(self, book, view, event):
		self.target_book = book
		self.target_view = view

		tabs = book.get_tabs()

		# Select menu

		visible = bool(tabs)

		ui = get_ui_manager()

		item = ui.get_widget('/book-popup/select-view')
		item.set_property('visible', visible)

		if visible:
			menu = item.get_submenu()

			for item in menu.get_children():
				menu.remove(item)
				item.destroy()

			for tab in tabs:
				text = str(tab)

				item = gtk.MenuItem(text)
				item.connect_after('activate', self.on_view_select_activate, tab)
				item.show()

				menu.append(item)

		# Delete item

		item = ui.get_widget('/book-popup/delete-view')
		item.set_property('visible', view is not None)

		# Show

		menu = ui.get_widget('/book-popup')
		menu.popup(None, None, None, event.button, event.time)

	# View operations

	def add_view(self, view, book=None, location=None):
		assert book or location

		if not book:
			book = self.get_book(location)

		widget = view.get_widget()
		widget.connect('destroy', self.on_view_destroy)

		book.append_view(view)

		self.books_by_view[view] = book
		self.views_by_widget[widget] = view

		return view

	def remove_view(self, view):
		try:
			view.cleanup()
		except Exception, e:
			print >>sys.stderr, e

		widget = view.get_widget()
		widget.destroy()

	def show_view(self, view, focus=False):
		book = self.books_by_view[view]
		book.set_view(view)

		if focus:
			widget = view.get_widget()
			widget.grab_focus()

	def on_view_create_activate(self, item, launcher):
		view = launcher.create()
		self.add_view(view, book=self.target_book)

	def on_view_select_activate(self, item, tab):
		self.target_book.set_tab(tab)

	def on_view_delete_activate(self, item):
		self.remove_view(self.target_view)
		self.target_view = None

	def on_view_destroy(self, widget):
		view = self.views_by_widget.pop(widget)
		self.books_by_view.pop(view)

	# Actions

	def on_about_activate(self, action):
		dialog = self.glade.get_widget('about_dialog')
		dialog.run()

	def on_preferences_activate(self, action):
		import preferences
		preferences.Preferences(datadir)

	def on_quit_activate(self, action):
		window = self.glade.get_widget('window')
		window.destroy()

	# Cleanup

	def cleanup(self):
		for view in self.books_by_view:
			try:
				view.cleanup()
			except Exception, e:
				print >>sys.stderr, e

		self.conf.remove_all()

	def on_window_destroy(self, window):
		self.emit('quit')

	def on_quit(self, env):
		self.cleanup()

gobject.type_register(Environment)
gobject.signal_new('quit', Environment, 0, gobject.TYPE_NONE, [])

# Settings

class Settings (settings.SettingsExtension):
	new_entry = '<New>'
	delete_entry = '<Delete>'

	def __init__(self):
		settings.SettingsExtension.__init__(self)

		path = os.path.join(datadir, 'encode', 'environment-settings.glade')
		self.glade = gtk.glade.XML(path)
		self.glade.signal_autoconnect(self)

		self.conf = config.Directory('encode')

		self._init_combo(HandlerExtension, 'handler_combobox')
		self._init_combo(OutputExtension, 'output_combobox')
		self._init_combo(DifferExtension, 'differ_combobox')
		self._init_views()

		self.dirty = set((
			'handler',
			'output',
			'differ',
			'views',
		))
		self.revert()

	def _init_combo(self, extension_type, combobox_name):
		store = self._get_classname_store(extension_type)
		combobox = self.glade.get_widget(combobox_name)
		combobox.set_model(store)

	def _init_views(self):
		store = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_STRING)

		treeview = self.glade.get_widget('views_treeview')
		treeview.set_model(store)

		self.view_classname_store = self._get_classname_store(view.ViewExtension)
		self.view_classname_store.append([self.delete_entry])

		render = gtk.CellRendererCombo()
		render.set_property('ypad', 0)
		render.set_property('model', self.view_classname_store)
		render.set_property('text-column', 0)
		render.set_property('editable', True)
		render.set_property('has-entry', False)
		render.connect('edited', self.on_views_edited, 0)
		column = gtk.TreeViewColumn(gettext('View'), render, text=0)
		treeview.append_column(column)

		self.view_location_store = gtk.ListStore(gobject.TYPE_STRING)
		self.view_location_store.append(['left'])
		self.view_location_store.append(['top'])
		self.view_location_store.append(['bottom'])

		render = gtk.CellRendererCombo()
		render.set_property('ypad', 0)
		render.set_property('model', self.view_location_store)
		render.set_property('text-column', 0)
		render.set_property('editable', True)
		render.set_property('has-entry', False)
		render.connect('edited', self.on_views_edited, 1)
		column = gtk.TreeViewColumn(gettext('Location'), render, text=1)
		treeview.append_column(column)

	def _get_classname_store(self, extension_type):
		store = gtk.ListStore(gobject.TYPE_STRING)
		for cls in extension.get_children(extension_type):
			name = '%s.%s' % (cls.__module__, cls.__name__)
			store.append([name])
		return store

	def _get_index(self, store, text):
		index = -1

		iter = store.get_iter_root()
		while iter:
			index += 1
			if store.get_value(iter, 0) == text:
				break
			iter = store.iter_next(iter)

		if not iter:
			index = -1

		return index

	def get_priority(self):
		return 1

	def get_name(self):
		return gettext('Defaults')

	def get_widget(self):
		return self.glade.get_widget('root')

	def apply(self):
		for key in ('handler', 'output', 'differ'):
			if key in self.dirty:
				combobox = self.glade.get_widget('%s_combobox' % key)
				name = combobox.get_active_text()
				self.conf.set_string('default_%s' % key, name)

		if 'views' in self.dirty:
			treeview = self.glade.get_widget('views_treeview')
			store = treeview.get_model()

			views = []
			iter = store.get_iter_root()
			while iter:
				name, location = store.get(iter, 0, 1)
				if name and location and name not in (self.new_entry, self.delete_entry):
					views.append('%s=%s' % (name, location))
				iter = store.iter_next(iter)

			self.conf.set_strings('views', views)

		self.dirty.clear()

	def revert(self):
		for key in ('handler', 'output', 'differ'):
			if key in self.dirty:
				combobox = self.glade.get_widget('%s_combobox' % key)
				store = combobox.get_model()

				name = self.conf.get_string('default_%s' % key)
				index = self._get_index(store, name)

				combobox.set_active(index)

		if 'views' in self.dirty:
			treeview = self.glade.get_widget('views_treeview')
			store = treeview.get_model()
			store.clear()

			for entry in self.conf.get_strings('views', []):
				store.append(entry.split('=', 1))

			store.append([self.new_entry, ''])

		self.dirty.clear()

	def on_handler_changed(self, combobox):
		self.dirty.add('handler')
		self.modified()

	def on_output_changed(self, combobox):
		self.dirty.add('output')
		self.modified()

	def on_differ_changed(self, combobox):
		self.dirty.add('differ')
		self.modified()

	def on_views_edited(self, cellrenderer, path, text, column):
		if not text:
			return

		treeview = self.glade.get_widget('views_treeview')
		store = treeview.get_model()
		iter = store.get_iter(path)

		if column == 0 and text == self.delete_entry:
			if store.iter_next(iter):
				store.remove(iter)

				self.dirty.add('views')
				self.modified()

			return

		if column == 1:
			name = store.get_value(iter, 0)
			if name == self.new_entry:
				return

		store.set_value(iter, column, text)

		if column == 0:
			name = text
			location = store.get_value(iter, 1)

			if not location:
				location = 'top'
				store.set_value(iter, 1, location)

			if not store.iter_next(iter):
				store.append([self.new_entry, ''])

		self.dirty.add('views')
		self.modified()

gobject.type_register(Settings)

# Extension types

class LauncherExtension (extension.Extension):
	'''View object factory.'''

	def __init__(self):
		extension.Extension.__init__(self)

	def get_action(self):
		'''Return a gtk.Action for creating Views.'''
		return None

	def create(self):
		'''Return a view instance.'''
		return None

gobject.type_register(LauncherExtension)

class HandlerExtension (extension.Extension):
	'''File viewer or editor.'''

	def __init__(self):
		extension.Extension.__init__(self)

	def get_action(self):
		'''Return a gtk.Action for opening files.'''
		return None

	def supports_file(self, path):
		'''Return True if the file type of PATH is supported by this handler.'''
		return True

	def open_file(self, path, workdir=None, line=None):
		'''Open file PATH for editing or bring the associated buffer to foreground if
		   one exists.  Attempt to move the cursor to LINE if it's
		   specified.  WORKDIR is the root of the project tree where
		   PATH is located (it may be used to update the handler's
		   state).'''
		pass

	def close_file(self, path):
		'''Close the buffer associated with PATH if one exists.'''
		pass

gobject.type_register(HandlerExtension)

class DifferExtension (extension.Extension):
	'''Difference viewer.'''

	def __init__(self):
		extension.Extension.__init__(self)

	def compare_files(self, original_data, modified_path):
		'''The contents of PATH ("modified version") are compared to the DATA string
		   ("original version").'''
		pass

gobject.type_register(DifferExtension)

class OutputExtension (extension.Extension):
	'''Command output viewer.'''

	def __init__(self):
		extension.Extension.__init__(self)

	def set_batch(self, batch):
		'''Associates BATCH with this object.  The batch's output callbacks should be
		   implemented.  The execution can be monitored via via
		   signals.  All ties to an old batch should be cut when a
		   new one is set.'''
		return None

gobject.type_register(OutputExtension)

# Exceptions

class EnvironmentError (Exception):
	def __init__(self, msg):
		Exception.__init__(self, msg)

class ViewNotFound (EnvironmentError):
	def __init__(self, msg):
		EnvironmentError.__init__(self, msg)

# UI instance

ui_manager = None

def get_ui_manager():
	global ui_manager
	if ui_manager is None:
		ui_manager = gtk.UIManager()
	return ui_manager

# Environment instance

instance = None

def get_instance():
	global instance
	assert instance is not None
	return instance

# Utilities

def add_ui(actions, filename, lifetime_widget):
	ui = get_ui_manager()

	ui.insert_action_group(actions, -1)
	merge_id = ui.add_ui_from_file(filename)

	def on_destroy(widget):
		ui.remove_ui(merge_id)
		ui.remove_action_group(actions)

	lifetime_widget.connect('destroy', on_destroy)

	return ui

def add_view(view, location):
	env = get_instance()
	env.add_view(view, None, location)

def show_view(view, focus=False):
	env = get_instance()
	env.show_view(view, focus)

def edit(path, workdir=None, line=None):
	env = get_instance()
	tuple = env.default_handler.open_file(path, workdir, line)
	if tuple:
		view, focus = tuple
		env.show_view(view, focus)

def execute(batch):
	env = get_instance()
	env.default_output.set_batch(batch)
	batch.start()

def compare(original_data, modified_path):
	env = get_instance()
	env.default_differ.compare_files(original_data, modified_path)

# Main

def main(datadir_param, version):
	def on_quit(object):
		gtk.main_quit()

	def show_preferences():
		conf = config.Directory('encode')

		if not conf.get_bool('preferences_shown', False):
			import preferences

			prefs = preferences.Preferences(datadir)
			prefs.connect('quit', on_quit)

			gtk.main()

			conf.set_bool('preferences_shown', True)

	try:
		global datadir
		datadir = datadir_param

		extension.load(None, 'encode.*', datadir)

		show_preferences()

		# Environment

		global instance
		instance = Environment(version)
		instance.connect('quit', on_quit)

		def on_signal():
			try:
				instance.cleanup()
			finally:
				sys.exit(1)

		signal.signal(signal.SIGTERM, on_signal)
		signal.signal(signal.SIGHUP, on_signal)

		gtk.main()

	except KeyboardInterrupt:
		pass
