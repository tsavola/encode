# Copyright 2005, 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import os
import signal
import gobject
import gtk
import gtk.glade
from gettext import gettext

import environment
import view
import config

datadir = None

def init_extension(d):
	global datadir
	datadir = d

##
## Launcher

class Launcher (environment.LauncherExtension):
	def __init__(self):
		environment.LauncherExtension.__init__(self)

		label = gettext('Xephyr X server')
		self.action = gtk.Action(None, label, None, None)
		self.action.set_visible(True)

	def get_action(self):
		return self.action

	def create(self):
		return View()

gobject.type_register(Launcher)

##
## View

class View (view.ViewExtension):
	command = 'Xephyr'
	options = []

	def __init__(self):
		view.ViewExtension.__init__(self)

		path = os.path.join(datadir, 'encode', 'xephyr.glade')
		self.glade = gtk.glade.XML(path)
		self.glade.signal_autoconnect(self)

		self.config = config.Directory('encode/xephyr')
		self.config.add_string ('command', self.set_command)
		self.config.add_strings('options', self.set_options)
		self.config.add_int    ('display', self.set_display)
		self.config.add_ints   ('size',    self.set_size)
		self.config.add_int    ('depth',   self.set_depth)
		self.config.add_int    ('dpi',     self.set_dpi)

		self.pid = None

	# Configuration

	def set_command(self, value):
		self.command = value

	def set_options(self, values):
		self.options = values

	def set_display(self, value):
		self._set_value('display_spin', value)

	def set_size(self, values):
		if len(values) == 2:
			width, height = values

			self._set_value('width_spin', width)
			self._set_value('height_spin', height)

			self._resize(width, height)

	def set_depth(self, value):
		self._set_value('depth_spin', value)

	def set_dpi(self, value):
		self._set_value('dpi_spin', value)

	# UI actions

	def on_run_toggled(self, button):
		start = button.get_active()
		if not (start ^ bool(self.pid)):
			return

		if start:
			parent = self._get_id()

			display = self._get_value('display_spin')
			width   = self._get_value('width_spin')
			height  = self._get_value('height_spin')
			depth   = self._get_value('depth_spin')
			dpi     = self._get_value('dpi_spin')

			display_arg = ':%d' % display
			parent_arg  = '%d' % parent
			screen_arg  = '%dx%dx%d' % (width, height, depth)
			dpi_arg     = '%d' % dpi

			args = [self.command, display_arg, '-parent', parent_arg, '-screen', screen_arg, '-dpi', dpi_arg]
			args += self.options

			try:
				self.pid = os.spawnvp(os.P_NOWAIT, self.command, args)
			except:
				button.set_active(False)
				raise
		else:
			try:
				os.kill(self.pid, signal.SIGTERM)
				os.waitpid(self.pid, 0)
			finally:
				self.pid = None

		for name in 'display_box', 'size_box', 'depth_box', 'dpi_box':
			widget = self.glade.get_widget(name)
			widget.set_sensitive(not start)

		self.notify('relocatable')

	# UI events

	def on_size_changed(self, spinbutton):
		width  = self._get_value('width_spin')
		height = self._get_value('height_spin')

		self._resize(width, height)

	def on_scrolledwindow_destroy(self, scrolledwindow):
		self.config.remove_all()

		if self.pid:
			os.kill(self.pid, signal.SIGTERM)
			os.waitpid(self.pid, 0)

	# View

	def is_relocatable(self):
		return self.pid is None

	def get_label(self):
		return self.glade.get_widget('title')

	def get_widget(self):
		return self.glade.get_widget('scrolledwindow')

	# Display

	def _get_display(self):
		if self.pid:
			return self._get_value('display_spin')
		else:
			return None

	# SpinButton helpers

	def _set_value(self, name, value):
		widget = self.glade.get_widget(name)
		widget.set_value(value)

	def _get_value(self, name):
		widget = self.glade.get_widget(name)
		return widget.get_value_as_int()

	# Placeholder helpers

	def _resize(self, width, height):
		viewport = self.glade.get_widget('viewport')
		viewport.set_size_request(width, height)

	def _get_id(self):
		placeholder = self.glade.get_widget('placeholder')
		return placeholder.window.xid

gobject.type_register(View)
