# Copyright 2005, 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import os
import gobject
import gtk
import gtk.glade
from gettext import gettext

import environment
import view
import execution
import extension
import config
import settings

DEFAULT_COMMAND = 'gvim'

datadir = None
views = []

def init_extension(d):
	global datadir
	datadir = d

##
## Handler

class Handler (environment.HandlerExtension):
	command = DEFAULT_COMMAND
	mode = None

	def __init__(self):
		environment.HandlerExtension.__init__(self)

		self.conf = config.Directory('encode/vim')
		self.conf.add_string('command', self.set_command)
		self.conf.add_string('mode', self.set_mode)

		self.executor = execution.Executor()

		label = gettext('Open in Vim')
		self.action = gtk.Action(None, label, None, None)
		self.action.set_visible(True)

	def set_command(self, command):
		self.command = command

	def set_mode(self, mode):
		self.mode = mode

	def get_view(self):
		global views
		if views:
			return views[0]
		else:
			raise environment.ViewNotFound, 'Vim not running'

	def _send(self, view, keys):
		server = view.get_server()
		args = [self.command, '--servername', server, '--remote-send', keys]

		self.executor.start(execution.Job(args))

	def _send_mode(self, view, keys):
		if self.mode:
			keys = keys + self.mode

		self._send(view, keys)

	def get_action(self):
		return self.action

	def supports_file(self, path):
		global views
		return bool(views)

	def open_file(self, path, workdir=None, line=None):
		view = self.get_view()

		keys = ''

		if workdir:
			keys += '<ESC>:cd %s<CR>' % escape(workdir)

		keys += '<ESC>:edit %s<CR>' % escape(path)

		if line is not None:
			keys += '%dgg' % line

		self._send_mode(view, keys)

		environment.show_view(view, True)

	def close_file(self, path):
		keys = '<ESC>:bdelete %s<CR>' % escape(path)

		global views
		for view in views:
			try:
				self._send_mode(view, keys)
			except:
				pass

	def cleanup(self):
		global views
		for view in views:
			self._send(view, '<ESC>:quit<CR>')

gobject.type_register(Handler)

##
## Launcher

class Launcher (environment.LauncherExtension):
	def __init__(self):
		environment.LauncherExtension.__init__(self)

		label = gettext('Vim text editor')
		self.action = gtk.Action(None, label, None, None)
		self.action.set_visible(True)

	def get_action(self):
		return self.action

	def create(self):
		return View()

gobject.type_register(Launcher)

##
## View

class View (view.Embedder):
	command = DEFAULT_COMMAND

	def __init__(self):
		view.Embedder.__init__(self)

		text = gettext('Vim')
		self.label = gtk.Label(text)
		self.label.show()

		self.conf = config.Directory('encode/vim')
		self.conf.add_string('command', self.set_command)

		self.executor = execution.Executor()
		self.server = None

	def set_command(self, command):
		self.command = command

	def get_server(self):
		return self.server

	def client_start(self, xid):
		self.server = 'ENCODE-%d' % xid

		args = [self.command,
		        '--nofork',
		        '--servername', self.server,
		        '--socketid', str(xid),
		        '-n']

		self.executor.start(execution.Job(args))

		global views
		views.append(self)

	def client_destroy(self):
		global views
		views.remove(self)

		self.server = None
		self.executor.stop()

	def get_label(self):
		return self.label

gobject.type_register(View)

def escape(path):
	# Backslash must be escaped first
	for c in '\\ $?!\'"&%#@*~|(){}[]':
		path = path.replace(c, '\\' + c)
	return path

##
## Settings

if 'set' not in __builtins__:
	import sets
	set = sets.Set

class Settings (settings.SettingsExtension):
	def __init__(self):
		settings.SettingsExtension.__init__(self)

		path = os.path.join(datadir, 'encode', 'vim-settings.glade')
		self.glade = gtk.glade.XML(path)
		self.glade.signal_autoconnect(self)

		self.conf = config.Directory('encode/vim')

		command = self.conf.get_string('command')

		if not os.path.isabs(command):
			fullcommand = find_binary(command)
			if fullcommand:
				command = fullcommand
				self.conf.set_string('command', command)

		filechooser = self.glade.get_widget('command_filechooser')
		filechooser.set_filename(command)

		self.dirty = set((
			'mode',
		))
		self.revert()

	def get_name(self):
		return gettext('Vim')

	def get_widget(self):
		return self.glade.get_widget('table')

	def apply(self):
		if 'command' in self.dirty:
			filechooser = self.glade.get_widget('command_filechooser')
			command = filechooser.get_filename()

			self.conf.set_string('command', command)

		if 'mode' in self.dirty:
			combobox = self.glade.get_widget('mode_combobox')
			index = combobox.get_active()

			if index == 1:
				mode = 'i'
			else:
				mode = ''

			self.conf.set_string('mode', mode)

		self.dirty.clear()

	def revert(self):
		if 'command' in self.dirty:
			command = self.conf.get_string('command')

			filechooser = self.glade.get_widget('command_filechooser')
			filechooser.set_filename(command)

		if 'mode' in self.dirty:
			mode = self.conf.get_string('mode')
			if mode == 'i':
				index = 1
			else:
				index = 0

			combobox = self.glade.get_widget('mode_combobox')
			combobox.set_active(index)

		self.dirty.clear()

	def on_command_changed(self, filechooser):
		new_command = filechooser.get_filename()
		old_command = self.conf.get_string('command')

		if new_command and new_command != old_command:
			self.dirty.add('command')
			self.modified()

	def on_mode_changed(self, combobox):
		self.dirty.add('mode')
		self.modified()

gobject.type_register(Settings)

def find_binary(name):
	for dir in os.environ['PATH'].split(':'):
		path = os.path.join(dir, name)
		if os.path.exists(path):
			return path
	return None
