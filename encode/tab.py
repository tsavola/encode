# Copyright 2005, 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import weakref
import gobject
import gtk

DRAG_TARGETS = (
	('_ENCODE_TAB_MOVE', gtk.TARGET_SAME_APP, 0),
)

drag_source = None

##
## Action Area

class ActionArea (gtk.EventBox):
	def __init__(self, visible_window=False):
		gtk.EventBox.__init__(self)

		self.set_visible_window(visible_window)
		self.set_above_child(False)

		self.connect('button-press-event', self.on_press)

	def on_press(self, area, event):
		if event.button == 1:
			self.emit('left-button-press', event)
		elif event.button == 3:
			self.emit('right-button-press', event)
		return False

gobject.type_register(ActionArea)
gobject.signal_new('left-button-press', ActionArea, 0, gobject.TYPE_NONE, [gtk.gdk.Event])
gobject.signal_new('right-button-press', ActionArea, 0, gobject.TYPE_NONE, [gtk.gdk.Event])

##
## Button

class Button (ActionArea):
	def __init__(self, widget):
		ActionArea.__init__(self, visible_window=True)

		align = gtk.Alignment(0.5, 0.5)
		align.show()

		self.add(align)
		align.add(widget)

		self.connect('enter-notify-event', self.on_enter)
		self.connect('leave-notify-event', self.on_leave)

	def on_enter(self, button, event):
		if self.state == gtk.STATE_NORMAL:
			self.set_state(gtk.STATE_PRELIGHT)
		return False

	def on_leave(self, button, event):
		if self.state == gtk.STATE_PRELIGHT:
			self.set_state(gtk.STATE_NORMAL)
		return False

gobject.type_register(Button)

##
## Tab

class Tab (Button):
	def __init__(self, view, x=0, y=0, angle=0):
		self.view = view

		label = view.get_label()
		label.set_angle(angle)

		Button.__init__(self, label)

		align = self.get_child()
		align.set_padding(y, y, x, x)

		self.connect('drag-begin', self.on_drag_begin)
		self.connect('drag-data-get', self.on_drag_get)
		self.update_drag_source()

		self.view_notify = self.view.connect_after('notify::relocatable', self.on_view_notify)

	def __str__(self):
		align = self.get_child()
		label = align.get_child()
		return label.get_label()

	def get_view(self):
		return self.view

	def pop_view(self):
		view = self.view
		self.view = None

		if view:
			align = self.get_child()
			self.remove(align)

			label = align.get_child()
			align.remove(label)

			view.disconnect(self.view_notify)
			del self.view_notify

		return view

	def update_drag_source(self):
		if self.view.is_relocatable():
			self.drag_source_set(gtk.gdk.BUTTON1_MASK, DRAG_TARGETS, gtk.gdk.ACTION_MOVE)
		else:
			self.drag_source_unset()

	def on_view_notify(self, view, property):
		self.update_drag_source()

	def on_drag_begin(self, tab, context):
		width, height = self.window.get_size()
		colormap = self.window.get_colormap()

		pixbuf = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB, False, 8, width, height)
		pixbuf = pixbuf.get_from_drawable(self.window, colormap, 0, 0, 0, 0, width, height)

		if pixbuf:
			x = width / 2
			y = height / 2

			context.set_icon_pixbuf(pixbuf, x, y)

	def on_drag_get(self, tab, context, selection, info, timestamp):
		global drag_source
		drag_source = weakref.ref(self)

gobject.type_register(Tab)
gobject.signal_new('selected', Tab, 0, gobject.TYPE_NONE, [])

class HTab (Tab):
	def __init__(self, view):
		Tab.__init__(self, view, x=6, y=1, angle=0)

gobject.type_register(HTab)

class LeftVTab (Tab):
	def __init__(self, view):
		Tab.__init__(self, view, x=1, y=6, angle=90)

gobject.type_register(LeftVTab)

class RightVTab (Tab):
	def __init__(self, view):
		Tab.__init__(self, view, x=1, y=6, angle=270)

gobject.type_register(RightVTab)

##
## Tab bars

class AbstractTabBar:
	def __init__(self):
		self.connect('add', self.on_add)

		area = ActionArea()
		area.show()

		self.add(area)
		self.set_child_packing(area, expand=True, fill=True, padding=0, pack_type=gtk.PACK_END)

		self.selected = None

	def add_tab(self, tab):
		# pack_start() doesn't seem to emit 'add' signal
		self.add(tab)
		self.set_child_packing(tab, expand=False, fill=True, padding=0, pack_type=gtk.PACK_START)

	def on_add(self, box, child):
		if isinstance(child, Tab):
			child.connect('left-button-press', self.on_tab_left)
			child.connect('right-button-press', self.on_tab_right)
			child.connect('destroy', self.on_tab_destroy)
		else:
			child.connect('right-button-press', self.on_child_right)

	def on_tab_left(self, tab, event):
		self.select(tab)

	def on_tab_right(self, tab, event):
		self.emit('tab-right-button-press', tab, event)

	def on_tab_destroy(self, tab):
		if tab == self.selected:
			self.selected = None

	def on_child_right(self, child, event):
		self.emit('tab-right-button-press', None, event)

	def select(self, tab):
		if tab != self.selected:
			old = self.selected
			self.selected = tab

			tab.set_state(gtk.STATE_SELECTED)

			if old:
				old.set_state(gtk.STATE_NORMAL)

			tab.emit('selected')

	def get_tabs(self):
		return [widget for widget in self.get_children() if isinstance(widget, Tab)]

class HTabBar (gtk.HBox, AbstractTabBar):
	def __init__(self):
		gtk.HBox.__init__(self)
		AbstractTabBar.__init__(self)

gobject.type_register(HTabBar)
gobject.signal_new('tab-right-button-press', HTabBar, 0, gobject.TYPE_NONE, [Tab, gtk.gdk.Event])

class VTabBar (gtk.VBox, AbstractTabBar):
	def __init__(self):
		gtk.VBox.__init__(self)
		AbstractTabBar.__init__(self)

gobject.type_register(VTabBar)
gobject.signal_new('tab-right-button-press', VTabBar, 0, gobject.TYPE_NONE, [Tab, gtk.gdk.Event])

##
## Tab books

class AbstractTabBook:
	def __init__(self, tabs_first=False):
		self.notebook = gtk.Notebook()
		self.notebook.set_property('show-border', False)
		self.notebook.set_property('show-tabs', False)
		self.notebook.show()

		self.pack_start(self.notebook, expand=True, fill=True)

		if self.bar_type:
			self.bar = self.bar_type()
			self.bar.connect('tab-right-button-press', self.on_tab_right)
			self.bar.show()

			self.pack_start(self.bar, expand=False, fill=True)

			if tabs_first:
				self.child_set_property(self.notebook, 'position', 1)
				self.child_set_property(self.bar, 'position', 0)

			self._set_expansion(tabs=True)

			self.notebook.connect('remove', self.on_notebook_remove)

			self.tabs_by_widget = {}

			flags = gtk.DEST_DEFAULT_MOTION | gtk.DEST_DEFAULT_HIGHLIGHT | gtk.DEST_DEFAULT_DROP
			self.bar.drag_dest_set(flags, DRAG_TARGETS, gtk.gdk.ACTION_MOVE)
			self.bar.connect('drag-drop', self.on_drag_drop)

		self.connect('next-view', self.on_change_view, 1)
		self.connect('previous-view', self.on_change_view, -1)

	def on_drag_drop(self, bar, context, x, y, timestamp):
		global drag_source
		source_tab = drag_source()
		drag_source = None

		source_bar = source_tab.get_parent()
		source_book = source_bar.get_parent()

		if self == source_book:
			self.bar.remove(source_tab)
			self.bar.add_tab(source_tab)
		else:
			view = source_tab.pop_view()
			source_book.remove_view(view)
			self.append_view(view)

		context.finish(True, False, timestamp)
		return True

	def append_view(self, view):
		widget = view.get_widget()
		self.notebook.append_page(widget)

		if self.bar_type:
			if self.notebook.get_n_pages() == 1:
				self._set_expansion(tabs=False)

			tab = self.tab_type(view)
			tab.connect('selected', self.on_tab_selected, widget)
			tab.show()

			self.tabs_by_widget[widget] = tab

			self.bar.add_tab(tab)
			self.bar.select(tab)

	def remove_view(self, view):
		widget = view.get_widget()
		self.notebook.remove(widget)

	def set_view(self, view):
		widget = view.get_widget()
		if self.bar_type:
			tab = self.tabs_by_widget[widget]
			self.bar.select(tab)
		else:
			num = self.notebook.page_num(widget)
			self.notebook.set_current_page(num)

	def first_view(self):
		count = self.notebook.get_n_pages()
		if count > 0:
			if self.bar_type:
				widget = self.notebook.get_nth_page(0)
				tab = self.tabs_by_widget[widget]
				self.bar.select(tab)
			else:
				self.notebook.set_current_page(0)

	def change_view(self, increment):
		count = self.notebook.get_n_pages()
		if count > 0:
			num = self.notebook.get_current_page()
			num = (num + increment) % count

			if self.bar_type:
				widget = self.notebook.get_nth_page(num)
				tab = self.tabs_by_widget[widget]
				self.bar.select(tab)
			else:
				self.notebook.set_current_page(num)

	def get_tabs(self):
		if self.bar_type:
			return self.bar.get_tabs()
		else:
			return []

	def set_tab(self, tab):
		if self.bar_type:
			self.bar.select(tab)

	def on_change_view(self, book, increment):
		self.change_view(increment)

	def on_notebook_remove(self, notebook, widget):
		tab = self.tabs_by_widget.pop(widget)
		tab.destroy()

		active_num = self.notebook.get_current_page()
		if active_num >= 0:
			active_widget = self.notebook.get_nth_page(active_num)
			active_tab = self.tabs_by_widget[active_widget]

			self.bar.select(active_tab)
		else:
			self._set_expansion(tabs=True)

	def on_tab_selected(self, tab, widget):
		num = self.notebook.page_num(widget)
		self.notebook.set_current_page(num)

	def on_tab_right(self, bar, tab, event):
		if tab:
			view = tab.get_view()
		else:
			view = None

		self.emit('tab-right-button-press', view, event)

	def _set_expansion(self, tabs):
		self.set_child_packing(self.notebook, not tabs, not tabs, 0, gtk.PACK_START)
		self.set_child_packing(self.bar, tabs, tabs, 0, gtk.PACK_START)

class TablessBook (gtk.HBox, AbstractTabBook):
	bar_type = None

	def __init__(self):
		gtk.HBox.__init__(self)
		AbstractTabBook.__init__(self)

gobject.type_register(TablessBook)
gobject.signal_new('next-view', TablessBook, gobject.SIGNAL_ACTION, gobject.TYPE_NONE, [])
gobject.signal_new('previous-view', TablessBook, gobject.SIGNAL_ACTION, gobject.TYPE_NONE, [])

class HTabBook (gtk.VBox, AbstractTabBook):
	tab_type = HTab
	bar_type = HTabBar

	def __init__(self, tabs_top=False):
		gtk.VBox.__init__(self)
		AbstractTabBook.__init__(self, tabs_top)

gobject.type_register(HTabBook)
gobject.signal_new('next-view', HTabBook, gobject.SIGNAL_ACTION, gobject.TYPE_NONE, [])
gobject.signal_new('previous-view', HTabBook, gobject.SIGNAL_ACTION, gobject.TYPE_NONE, [])
gobject.signal_new('tab-right-button-press', HTabBook, 0, gobject.TYPE_NONE, [gobject.GObject, gtk.gdk.Event])

class VTabBook (gtk.HBox, AbstractTabBook):
	bar_type = VTabBar

	def __init__(self, tabs_left=False):
		if tabs_left:
			self.tab_type = LeftVTab
		else:
			self.tab_type = RightVTab

		gtk.HBox.__init__(self)
		AbstractTabBook.__init__(self, tabs_left)

gobject.type_register(VTabBook)
gobject.signal_new('next-view', VTabBook, gobject.SIGNAL_ACTION, gobject.TYPE_NONE, [])
gobject.signal_new('previous-view', VTabBook, gobject.SIGNAL_ACTION, gobject.TYPE_NONE, [])
gobject.signal_new('tab-right-button-press', VTabBook, 0, gobject.TYPE_NONE, [gobject.GObject, gtk.gdk.Event])
