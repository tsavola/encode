# Copyright 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import os
import stat
import tempfile
import gobject

import environment
import execution
import config

conf = config.Directory('encode/externaldiff')

class Differ (environment.DifferExtension):
	def compare_files(self, original_data, modified_path):
		command = conf.get_strings('command')
		if not command:
			return

		dirname = tempfile.mkdtemp()
		filename = os.path.basename(modified_path)
		original_path = os.path.join(dirname, filename)

		original_file = open(original_path, 'wb')
		original_file.write(original_data)
		original_file.close()

		os.chmod(original_path, stat.S_IRUSR)

		args = command + [original_path, modified_path]
		job = execution.Job(args)

		executor = execution.Utility()
		executor.connect('stopped', self.on_executor_stop, original_path)
		executor.without_output(job)

	def on_executor_stop(self, executor, status, original_path):
		os.chmod(original_path, stat.S_IRUSR | stat.S_IWUSR)
		os.unlink(original_path)

		dirname = os.path.dirname(original_path)
		try:
			os.rmdir(dirname)
		except:
			pass

gobject.type_register(Differ)
