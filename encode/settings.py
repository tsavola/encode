# Copyright 2005, 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import gobject
import extension

class SettingsExtension (extension.Extension):
	'''Preferences GUI element.'''

	accept = True

	def __init__(self):
		extension.Extension.__init__(self)

	def get_priority(self):
		return -1

	def get_name(self):
		return None

	def get_widget(self):
		'''Return the gtk.Widget which is the root of the widget hierarchy that
		   should be attached to the preferences window.  The
		   settings object is responsible for setting the "visible"
		   property of the widget.'''
		return None

	def apply(self):
		'''Save all values that have been changed by the user.'''
		pass

	def revert(self):
		'''Restore the user interface to the saved state.'''
		pass

	def modified(self):
		'''Emits the "modified" signal.'''
		self.emit('modified')

gobject.type_register(SettingsExtension)
gobject.signal_new('modified', SettingsExtension, 0, gobject.TYPE_NONE, [])
