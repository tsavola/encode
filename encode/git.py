# Copyright 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import os
import re
import filecmp
import gobject
from cStringIO import StringIO

import filesystem
import execution

class Controller (filesystem.Controller):
	rules = ('.git',)

	re_sha1 = re.compile('^[[:digit:]]+[[:space:]]+blob[[:space:]]+([[:xdigit:]+)[[:space:]]')

	def __init__(self, path):
		filesystem.Controller.__init__(self, path)
		self.execute = execution.Utility(error_type=GitError)

	def is_controlled(self, name):
		return bool(self._call_output(['ls-files', '-c', '--', name]))

	def is_modified(self, name):
		return bool(self._call_output(['ls-files', '-m', '--', name]))

	def get_latest(self, name):
		sha1 = self._get_sha1(name)
		if sha1 is not None:
			return self._call_output(['cat-file', 'blob', sha1])
		else:
			return None

	def control(self, name, recursive=False):
		path = os.path.join(self.root, name)
		if os.path.isdir(path):
			if recursive:
				for filename in os.listdir(path):
					if filename != '.' and filename != '..':
						child_name = os.path.join(name, filename)
						self.control(child_name, True)
		else:
			self._call(['update-index', '--add', '--', name])

	def release(self, name):
		output = self._call_output(['ls-files', '-c', '--', name])
		lines = output.strip().split('\n')
		self._call(['update-index', '--force-remove', '--'] + lines)

	def make_file(self, name, control=True):
		filesystem.Controller.make_file(self, name)
		if control:
			self.control(name)

	def make_directory(self, name, control=True):
		filesystem.Controller.make_directory(self, name)
		if control:
			self.control(name)

	def remove(self, name, control=True):
		filesystem.Controller.remove(self, name)
		if control:
			self.release(name)

	def rename(self, old_name, new_name, control=True):
		if control:
			new_path = os.path.join(self.root, new_name)
			if not os.path.isdir(new_path):
				self._call(['mv', old_name, new_name])
		else:
			filesystem.Controller.rename(self, old_name, new_name)

	def clone(self, old_name, new_name, control=True):
		filesystem.Controller.clone(self, old_name, new_name)
		if control:
			self.control(name)

	def _get_sha1(self, name):
		output = self._call_output(['ls-tree', 'master', name])
		match = self.re_sha1.match(output)
		if match:
			return match.group(1)
		else:
			return None

	def _get_job(self, params):
		args = ['git'] + params
		return execution.Job(args, workdir=self.root)

	def _call(self, params):
		job = self._get_job(params)
		self.execute.without_output(job)

	def _call_output(self, params):
		job = self._get_job(params)
		return self.execute.with_output(job)

gobject.type_register(Controller)

class GitError (execution.UtilityError):
	def __init__(self, message):
		execution.UtilityError.__init__(self, message)
