# Copyright 2005, 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import os
import stat
import re
import glob
import shutil
import gobject
import pango
import gtk
import gnomevfs
from gettext import gettext

import environment
import view
import scroll
import config
import execution
import extension

if 'frozenset' not in __builtins__:
	import sets
	frozenset = sets.ImmutableSet

# os.path.expanduser() is not used to implement tilde_to_home() because
# it must be symmetic with home_to_tilde() and expanduser() also translates
# ~user which home_to_tilde() does not

bloated = os.environ['HOME'] + os.sep
compact = '~' + os.sep

def tilde_to_home(path):
	if path.startswith(compact):
		return path.replace(compact, bloated, 1)
	else:
		return path

def home_to_tilde(path):
	if path.startswith(bloated):
		return path.replace(bloated, compact, 1)
	else:
		return path

def copy_hierarchy(old, new):
	if os.path.isdir(old):
		shutil.copytree(old, new)
	else:
		shutil.copy(old, new)

def construct_components(store, iter):
	components = []

	while iter:
		file = store.get_value(iter, 0)
		components.insert(0, file)

		iter = store.iter_parent(iter)

	return components

def construct_path(store, iter):
	components = construct_components(store, iter)

	path = ''
	for c in components:
		path = os.path.join(path, c)

	return tilde_to_home(path)

def construct_project_name(store, iter):
	components = construct_components(store, iter)

	project = components[0]

	name = ''
	for c in components[1:]:
		name = os.path.join(name, c)

	return tilde_to_home(project), name

class StickySelections (gtk.GenericCellRenderer):
	__gproperties__ = {
		'index': (gobject.TYPE_INT,
		          'Build order',
		          'Index of the build target',
			  -1,
			  0x7fffffff,
			  -1,
		          gobject.PARAM_WRITABLE),
	}

	def __init__(self):
		gtk.GenericCellRenderer.__init__(self)
		self.set_property('mode', gtk.CELL_RENDERER_MODE_ACTIVATABLE)
		self.set_property('xpad', 1)
		self.set_property('ypad', 0)

		self.layouts = [None, None]
		self.references = []
		self.index = -1

	def refresh(self, store):
		index = 0

		while index < len(self.references):
			ref = self.references[index]

			if ref.valid():
				index += 1

				path = ref.get_path()
				iter = store.get_iter(path)
				store.set_value(iter, 3, index)
			else:
				del self.references[index]

	def set_paths(self, store, paths):
		for ref in self.references:
			if ref.valid():
				path = ref.get_path()
				iter = store.get_iter(path)
				store.set_value(iter, 3, 0)

		self.references = []
		index = 1

		for path in paths:
			ref = gtk.TreeRowReference(store, path)
			self.references.append(ref)

			iter = store.get_iter(path)
			store.set_value(iter, 3, index)
			index += 1

	def get_paths(self):
		return [ref.get_path() for ref in self.references]

	def do_set_property(self, property, value):
		if property.name == 'index':
			self.index = value
		else:
			gtk.GenericCellRenderer.do_set_property(self, property, value)

	def on_get_size(self, widget, cell_area):
		if self.index < 0:
			return 0, 0, 0, 0

		context = widget.get_pango_context()
		language = context.get_language()
		metrics = context.get_metrics(widget.style.font_desc, language)

		chars = len(str(len(self.references)))
		width = metrics.get_approximate_digit_width() * chars
		width = pango.PIXELS(width)

		height = metrics.get_ascent() + metrics.get_descent()
		height = pango.PIXELS(height)

		width += self.get_property('xpad') * 2
		height += self.get_property('ypad') * 2

		if cell_area:
			width = max(width, cell_area[2])
			height = max(height, cell_area[3])

		return 0, 0, width, height

	def on_render(self, window, widget, background_area, cell_area, expose_area, flags):
		if self.index < 0:
			return

		style = widget.style

		if flags & gtk.CELL_RENDERER_PRELIT:
			x, y, w, h = background_area

			style.paint_flat_box(window, gtk.STATE_SELECTED, gtk.SHADOW_NONE,
			                     expose_area, widget, None, x, y, w, h)

		if self.index > 0:
			inverted = bool(flags & (gtk.CELL_RENDERER_SELECTED | gtk.CELL_RENDERER_PRELIT))

			layout = self._get_layout(widget, inverted)
			layout.set_text(str(self.index))

			cell_x, cell_y, cell_w, cell_h = cell_area

			layout_w, layout_h = layout.get_pixel_size()

			x = cell_x + (cell_w - layout_w) - self.get_property('xpad')
			y = cell_y + (cell_h - layout_h) / 2

			style.paint_layout(window, gtk.STATE_NORMAL, True,
			                   expose_area, widget, None, x, y, layout)

	def on_activate(self, event, widget, path, background_area, cell_area, flags):
		if self.index < 0:
			return False

		store = widget.get_model()
		iter = store.get_iter(path)
		index = store.get_value(iter, 3)

		if index > 0:
			store.set_value(iter, 3, 0)
			del self.references[index - 1]

			for i in xrange(len(self.references)):
				ref = self.references[i]
				assert ref.valid()

				iter = store.get_iter(ref.get_path())
				store.set_value(iter, 3, i + 1)
		else:
			i = len(self.references)
			store.set_value(iter, 3, i + 1)

			ref = gtk.TreeRowReference(store, path)
			self.references.append(ref)

		return True

	def _get_layout(self, widget, inverted):
		if inverted:
			index = 1
		else:
			index = 0

		layout = self.layouts[index]

		if layout is None:
			if inverted:
				r = 65535
				g = 65535
				b = 65535
			else:
				color = widget.style.bg[gtk.STATE_SELECTED]
				r = color.red
				g = color.green
				b = color.blue

			attrs = pango.AttrList()
			attrs.insert(pango.AttrWeight(pango.WEIGHT_BOLD, 0, -1))
			attrs.insert(pango.AttrForeground(r, g, b, 0, -1))

			context = widget.get_pango_context()

			layout = pango.Layout(context)
			layout.set_attributes(attrs)

			self.layouts[index] = layout

		return layout

gobject.type_register(StickySelections)

class TreeData (gobject.GObject):
	def __init__(self):
		gobject.GObject.__init__(self)

		self.monitor = None
		self.controller = None

	def cleanup(self):
		if self.monitor is not None:
			gnomevfs.monitor_cancel(self.monitor)
			self.monitor = None

		if self.controller:
			self.controller.destroy()
			self.controller = None

gobject.type_register(TreeData)

PLACEHOLDER = ('', False, None, -1)

datadir = None

def init_extension(d):
	global datadir
	datadir = d

class View (view.ViewExtension):
	def __init__(self):
		view.ViewExtension.__init__(self)

		self.last_run_batch = None

		# Label

		text = gettext('Project')
		self.label = gtk.Label(text)
		self.label.show()

		# Glade

		path = os.path.join(datadir, 'encode', 'project.glade')
		self.glade = gtk.glade.XML(path)
		self.glade.signal_autoconnect(self)

		# Tree

		treestore = gtk.TreeStore(
			gobject.TYPE_STRING,   # name
			gobject.TYPE_BOOLEAN,  # is a directory?
			gobject.TYPE_OBJECT,   # TreeData
			gobject.TYPE_INT)      # StickySelections index

		treeview = self.glade.get_widget('treeview')
		treeview.set_model(treestore)

		self.sticky_selections = StickySelections()
		column = gtk.TreeViewColumn(None, self.sticky_selections)
		column.add_attribute(self.sticky_selections, 'index', 3)
		treeview.append_column(column)

		render = gtk.CellRendererText()
		render.set_property('ypad', 0)
		render.set_property('background-gdk', treeview.style.bg[gtk.STATE_PRELIGHT])
		column = gtk.TreeViewColumn(None, render)
		column.add_attribute(render, 'text', 0)
		column.add_attribute(render, 'background-set', 1)
		treeview.append_column(column)
		treeview.set_expander_column(column)

		# Scroller

		self.scroller = scroll.VScroller(treeview)
		self.scroller.connect('destroy', self.on_scroller_destroy)
		self.scroller.show()

		# Drag-and-drop

		targets = (
			('_ENCODE_PROJECT_MOVE', gtk.TARGET_SAME_WIDGET, 0),
			('text/uri-list', 0, 10),
			('_NETSCAPE_URL', 0, 11),
		)
		treeview.enable_model_drag_source(gtk.gdk.BUTTON1_MASK, targets, gtk.gdk.ACTION_COPY)
		treeview.enable_model_drag_dest(targets, gtk.gdk.ACTION_COPY)

		treeview.connect('drag-data-get', self.on_treeview_get)
		treeview.connect('drag-data-received', self.on_treeview_received)

		# UI

		group = gtk.ActionGroup('encode-project')

		action = gtk.Action('project', gettext('Project'), None, None)
		group.add_action(action)

		action = gtk.Action('add-tree', gettext('_Add directories'), None, gtk.STOCK_ADD)
		action.connect_after('activate', self.on_tree_add_activate)
		group.add_action(action)

		action = gtk.Action('build-selected', gettext('_Build selected'), None, gtk.STOCK_EXECUTE)
		action.connect_after('activate', self.on_selected_build_activate)
		group.add_action_with_accel(action, 'F7')

		action = gtk.Action('clean-selected', gettext('_Clean selected'), None, gtk.STOCK_CLEAR)
		action.connect_after('activate', self.on_selected_clean_activate)
		group.add_action_with_accel(action, 'F6')

		action = gtk.Action('run-last', gettext('_Run last'), None, gtk.STOCK_MEDIA_PLAY)
		action.connect_after('activate', self.on_last_run_activate)
		group.add_action_with_accel(action, 'F8')

		action = gtk.Action('build-tree', gettext('_Build'), None, gtk.STOCK_EXECUTE)
		action.connect_after('activate', self.on_tree_build_activate)
		group.add_action(action)

		action = gtk.Action('clean-tree', gettext('_Clean'), None, gtk.STOCK_CLEAR)
		action.connect_after('activate', self.on_tree_clean_activate)
		group.add_action(action)

		action = gtk.Action('remove-tree', gettext('_Remove from Project'), None, gtk.STOCK_REMOVE)
		action.connect_after('activate', self.on_tree_remove_activate)
		group.add_action(action)

		action = gtk.Action('build-dir', gettext('_Build'), None, gtk.STOCK_EXECUTE)
		action.connect_after('activate', self.on_dir_build_activate)
		group.add_action(action)

		action = gtk.Action('clean-dir', gettext('_Clean'), None, gtk.STOCK_CLEAR)
		action.connect_after('activate', self.on_dir_clean_activate)
		group.add_action(action)

		action = gtk.Action('control-dir', gettext('Control'), None, gtk.STOCK_ADD)
		action.connect_after('activate', self.on_control_activate)
		group.add_action(action)

		action = gtk.Action('control-recursive', gettext('Control All'), None, gtk.STOCK_ADD)
		action.connect_after('activate', self.on_control_recursive_activate)
		group.add_action(action)

		action = gtk.Action('release-dir', gettext('Release All'), None, gtk.STOCK_REMOVE)
		action.connect_after('activate', self.on_release_activate)
		group.add_action(action)

		action = gtk.Action('run-file', gettext('_Run'), None, gtk.STOCK_MEDIA_PLAY)
		action.connect_after('activate', self.on_file_run_activate)
		group.add_action(action)

		action = gtk.Action('compare-file', gettext('_Compare with Latest'), None, gtk.STOCK_DIALOG_INFO)
		action.connect_after('activate', self.on_compare_activate)
		group.add_action(action)

		action = gtk.Action('control-file', gettext('Control'), None, gtk.STOCK_ADD)
		action.connect_after('activate', self.on_control_activate)
		group.add_action(action)

		action = gtk.Action('release-file', gettext('Release'), None, gtk.STOCK_REMOVE)
		action.connect_after('activate', self.on_release_activate)
		group.add_action(action)

		path = os.path.join(datadir, 'encode', 'project.ui')
		self.ui = environment.add_ui(group, path, self.get_widget())

		# Actions

		item = self.ui.get_widget("/menubar/project/run-last")
		item.set_sensitive(False)

		# Popup hooks

		menu = self.ui.get_widget('/dir-popup')
		menu.connect('show', self.on_dir_popup_show)

		menu = self.ui.get_widget('/file-popup')
		menu.connect('show', self.on_file_popup_show)

		# Handler

		handlers_by_label = []

		for handler in extension.get_singleton_children(environment.HandlerExtension):
			action = handler.get_action()
			label = action.get_property('label')
			handlers_by_label.append((label, handler))

		handlers_by_label.sort()

		menu = self.ui.get_widget('/file-popup')
		self.handlers = []

		for label, handler in handlers_by_label:
			action = handler.get_action()
			action.connect_after('activate', self.on_handler_action_activate, handler)

			item = action.create_menu_item()
			menu.append(item)

			self.handlers.append((handler, item))

		# Configuration

		self.conf = config.Directory('encode/project')
		self.conf.add_strings('contents', self._set_contents)
		self.conf.add_strings('selections', self._set_sticky_selections)

		hidden = self.conf.get_string('hidden', '')
		self.hidden = re.compile(hidden)

	# Configuration

	def _store_contents(self, paths):
		self.conf.set_strings('contents', paths)

	def _set_sticky_selections(self, names):
		store = self._get_store()
		paths = []

		for name in names:
			iter = store.get_iter_root()
			while iter:
				if store.get_value(iter, 0) == name:
					path = store.get_path(iter)
					paths.append(path)

				iter = store.iter_next(iter)

		self.sticky_selections.set_paths(store, paths)

	# Content

	def _set_contents(self, paths):
		store = self._get_store()

		root_iter = store.get_iter_root()
		while root_iter:
			root_name = store.get_value(root_iter, 0)

			prev_iter = root_iter
			root_iter = store.iter_next(root_iter)

			if root_name not in paths:
				store.remove(prev_iter)

		for path in paths:
			name = home_to_tilde(path)

			exists = False

			root_iter = store.get_iter_root()
			while root_iter:
				root_name = store.get_value(root_iter, 0)

				if root_name == name:
					exists = True
					break

				if root_name > name:
					break

				root_iter = store.iter_next(root_iter)

			if not exists:
				iter = store.insert_before(None, root_iter, (name, True, None, 0))
				store.append(iter, PLACEHOLDER)

		self.sticky_selections.refresh(store)

	def _get_contents(self):
		paths = []

		store = self._get_store()

		iter = store.get_iter_root()
		while iter:
			name = store.get_value(iter, 0)
			paths.append(name)

			iter = store.iter_next(iter)

		return paths

	def _open_directory(self, store, iter):
		data = store.get_value(iter, 2)
		if not data:
			data = TreeData()
			store.set_value(iter, 2, data)

			path = construct_path(store, iter)

			treepath = store.get_path(iter)
			ref = gtk.TreeRowReference(store, treepath)

			uri = gnomevfs.get_uri_from_local_path(path)
			data.monitor = gnomevfs.monitor_add(uri, gnomevfs.MONITOR_DIRECTORY, self.on_directory_changed, ref)

			for file in os.listdir(path):
				self._update_directory(path, ref, file, None)

			if not store.iter_parent(iter):
				options = self._create_options(path)
				data.controller = self._create_controller(path, options)

	def _close_directory(self, store, dir_iter):
		if dir_iter:
			data = store.get_value(dir_iter, 2)
			if data:
				data.cleanup()
				store.set_value(dir_iter, 2, None)

		iter = store.iter_children(dir_iter)
		while iter:
			is_dir = store.get_value(iter, 1)
			if is_dir:
				self._close_directory(store, iter)

			next = store.iter_next(iter)
			store.remove(iter)
			iter = next

	# Tree utilities

	def _get_selection(self):
		store, iter, root_iter = self._get_selection_iters()
		root_path = construct_path(store, root_iter)
		root_path = tilde_to_home(root_path)

		return store, iter, root_path

	def _get_selection_iters(self):
		treeview = self.glade.get_widget('treeview')
		treepath, column = treeview.get_cursor()

		store = treeview.get_model()
		iter = store.get_iter(treepath)
		root_iter = store.get_iter(treepath[:1])

		return store, iter, root_iter

	def _get_store(self):
		treeview = self.glade.get_widget('treeview')
		return treeview.get_model()

	def _get_selected(self):
		treeview = self.glade.get_widget('treeview')
		store, iter = treeview.get_selection().get_selected()

		path = construct_path(store, iter)

		treepath = store.get_path(iter)
		root_treepath = treepath[:1]
		root_iter = store.get_iter(root_treepath)

		root_path = construct_path(store, root_iter)

		return root_path, path

	# Resources

	def _create_options(self, root):
		pair = self._find_project_extension(root, OptionsExtension, 'options_rules')
		if pair:
			eclass, match = pair
		else:
			classname = self.conf.get_string('default_options')
			eclass = extension.get_class(classname)
			match = None

		return eclass(root, match)

	def _create_controller(self, root, options):
		classname = options.get_controller_name()
		if classname:
			eclass = extension.get_class(classname)
		else:
			pair = self._find_project_extension(root, ControllerExtension)
			if pair:
				eclass = pair[0]
			else:
				classname = self.conf.get_string('default_controller')
				eclass = extension.get_class(classname)

		return eclass(root)

	def _find_project_extension(self, root, etype, rulekey=None):
		dirname = os.path.basename(root)

		def check(rule):
			name = rule.replace('$(DIRNAME)', dirname)
			pattern = os.path.join(root, name)
			paths = glob.glob(pattern)
			if paths:
				return paths[0]
			else:
				return None

		if rulekey:
			entries = self.conf.get_strings(rulekey)
			if entries:
				for entry in entries:
					classname, rules = entry.split('=')
					for rule in rules.split(':'):
						path = check(rule)
						if path:
							eclass = extension.get_class(classname)
							return eclass, path

		for eclass in extension.get_children(etype):
			if eclass.rules:
				for rule in eclass.rules:
					path = check(rule)
					if path:
						return eclass, path

		return None

	def _get_options(self, store, iter):
		treepath = store.get_path(iter)
		data = self._get_tree_data(store, treepath)
		if data:
			return data.options
		else:
			return None

	def _get_controller(self, store, treepath):
		data = self._get_tree_data(store, treepath)
		if data:
			return data.controller
		else:
			return None

	def _get_tree_data(self, store, treepath):
		root_treepath = treepath[:1]
		root_iter = store.get_iter(root_treepath)
		return store.get_value(root_iter, 2)

	# Filesystem

	def on_directory_changed(self, dir_uri, file_uri, event, dir_ref):
		dir_path = gnomevfs.get_local_path_from_uri(dir_uri)

		file_path = gnomevfs.get_local_path_from_uri(file_uri)
		file_name = os.path.basename(file_path)

		self._update_directory(dir_path, dir_ref, file_name, event)

	def _update_directory(self, dir_path, dir_ref, file, event):
		if self.hidden.match(file) is not None:
			return

		store = self._get_store()

		dir_treepath = dir_ref.get_path()
		dir_iter = store.get_iter(dir_treepath)

		if event is None or event == gnomevfs.MONITOR_EVENT_CREATED:
			path = os.path.join(dir_path, file)
			if not os.path.exists(path):
				return

			is_dir = os.path.isdir(path)

			sib_iter = store.iter_children(dir_iter)
			while sib_iter:
				sib_file = store.get_value(sib_iter, 0)

				if sib_file == file:
					sib_is_dir = store.get_value(sib_iter, 1)
					if sib_is_dir != is_dir:
						store.set_value(sib_iter, 1, is_dir)
						if is_dir:
							store.append(sib_iter, PLACEHOLDER)
						else:
							self._close_directory(store, sib_iter)
					return

				if sib_file > file:
					break

				sib_iter = store.iter_next(sib_iter)

			iter = store.insert_before(dir_iter, sib_iter, (file, is_dir, None, -1))
			if is_dir:
				store.append(iter, PLACEHOLDER)

			child_count = store.iter_n_children(dir_iter)
			if child_count == 2:
				iter = store.iter_children(dir_iter)
				tmp_name = store.get_value(iter, 0)
				if tmp_name == PLACEHOLDER[0]:
					store.remove(iter)

		elif event == gnomevfs.MONITOR_EVENT_DELETED:
			iter = store.iter_children(dir_iter)
			while iter:
				row_file = store.get_value(iter, 0)
				if row_file == file:
					break
				iter = store.iter_next(iter)

			if iter:
				child_count = store.iter_n_children(dir_iter)
				if child_count == 1:
					store.prepend(dir_iter, PLACEHOLDER)

				is_dir = store.get_value(iter, 1)
				if is_dir:
					self._close_directory(store, iter)

				store.remove(iter)

	# Add/Remove

	def on_tree_add_activate(self, item):
		dialog = self.glade.get_widget('tree_add_dialog')
		dialog.show()

	def on_tree_add_response(self, dialog, response):
		if response == gtk.RESPONSE_OK:
			old_paths = self._get_contents()
			new_paths = dialog.get_filenames()

			paths = list(frozenset(old_paths) | frozenset(new_paths))
			self._store_contents(paths)

		dialog.hide()

	def on_tree_remove_activate(self, item):
		store, iter, root_iter = self._get_selection_iters()
		path = store.get_value(root_iter, 0)

		paths = self._get_contents()
		paths.remove(path)

		self._store_contents(paths)

	# Drag-and-drop

	def on_treeview_get(self, treeview, context, selection, target, timestamp):
		treeselection = treeview.get_selection()
		store, iter = treeselection.get_selected()

		if target == 0:
			data = store.get_string_from_iter(iter)
		else:
			data = 'file://' + construct_path(store, iter)

		selection.set(selection.target, 8, data)

	def on_treeview_received(self, treeview, context, x, y, selection, target, timestamp):
		success = False

		store = treeview.get_model()
		row = treeview.get_dest_row_at_pos(x, y)
		if row:
			treepath, position = row

			if target == 0:
				iter = store.get_iter_from_string(selection.data)
				success = self._move_dragged(store, iter, treepath)
			else:
				for line in selection.data.split('\n'):
					line = line.rstrip('\r')
					components = line.split()
					uri = components[0]

					success = self._copy_dragged(store, uri, treepath)
					if not success:
						break

		context.finish(success, False, timestamp)

	def _move_dragged(self, store, source_iter, target_treepath):
		tmp_name = store.get_value(source_iter, 0)
		if tmp_name == PLACEHOLDER[0]:
			return False

		source_project, source_name = construct_project_name(store, source_iter)
		if not source_name:
			# TODO: display "Projects cannot be moved" error message?
			return False

		target_project, target_dir = self._get_drag_target(store, target_treepath)

		filename = os.path.basename(source_name)
		target_name = os.path.join(target_dir, filename)

		if source_project == target_project:
			controller = self._get_controller(store, target_treepath)

			do_control = controller.is_controlled(source_name)
			controller.rename(source_name, target_name, do_control)
		else:
			source_treepath = store.get_path(source_iter)
			controller = self._get_controller(store, source_treepath)

			do_control = controller.is_controlled(source_name)
			if do_control:
				controller.release(source_name)

			source_path = os.path.join(source_project, source_name)
			target_path = os.path.join(target_project, target_name)
			os.rename(source_path, target_path)

			if do_control:
				controller = self._get_controller(store, target_treepath)
				controller.seize_hierarchy(target_name)

		return True

	def _copy_dragged(self, store, source_uri, target_treepath):
		prefix = 'file://'
		hostname = 'localhost'

		if not source_uri.startswith(prefix):
			return False

		source_path = source_uri[len(prefix):]

		if source_path.startswith(hostname):
			source_path = source_path[len(hostname):]

		target_project, target_dir = self._get_drag_target(store, target_treepath)

		filename = os.path.basename(source_path)
		target_name = os.path.join(target_dir, filename)

		target_path = os.path.join(target_project, target_name)
		copy_hierarchy(source_path, target_path)

		control = self._get_controller(store, target_treepath)
		if control.is_controlled(target_dir):
			control.seize_hierarchy(target_name)

		return True

	def _get_drag_target(self, store, treepath):
		iter = store.get_iter(treepath)
		project, directory = construct_project_name(store, iter)

		path = os.path.join(project, directory)
		if not os.path.isdir(path):
			directory = os.path.dirname(directory)

		return project, directory

	# Popups

	def on_dir_popup_show(self, menu):
		treeview = self.glade.get_widget('treeview')
		selection = treeview.get_selection()
		store, iter = selection.get_selected()

		project, name = construct_project_name(store, iter)
		path = os.path.join(project, name)

		# Control

		treepath = store.get_path(iter)
		controller = self._get_controller(store, treepath)

		controlled = controller.is_controlled(name)

		for name in ('/dir-popup/control-dir', '/dir-popup/control-recursive'):
			item = self.ui.get_widget(name)
			item.set_property('visible', not controlled)

		item = self.ui.get_widget('/dir-popup/release-dir')
		item.set_property('visible', controlled)

	def on_file_popup_show(self, menu):
		treeview = self.glade.get_widget('treeview')
		selection = treeview.get_selection()
		store, iter = selection.get_selected()

		project, name = construct_project_name(store, iter)
		path = os.path.join(project, name)

		# Handlers

		for handler, item in self.handlers:
			support = handler.supports_file(path)
			item.set_property('visible', support)

		# Run

		result = os.stat(path)
		mode = stat.S_IMODE(result.st_mode)
		executable = bool(mode & 0111)

		item = self.ui.get_widget('/file-popup/run-file')
		item.set_sensitive(executable)

		# Control

		treepath = store.get_path(iter)
		controller = self._get_controller(store, treepath)

		controlled = controller.is_controlled(name)
		if controlled:
			modified = controller.is_modified(name)
		else:
			modified = False

		item = self.ui.get_widget('/file-popup/compare-file')
		item.set_property('visible', controlled)
		item.set_sensitive(modified)

		item = self.ui.get_widget('/file-popup/control-file')
		item.set_property('visible', not controlled)

		item = self.ui.get_widget('/file-popup/release-file')
		item.set_property('visible', controlled)

	def on_handler_action_activate(self, item, handler):
		treeview = self.glade.get_widget('treeview')
		selection = treeview.get_selection()
		store, iter = selection.get_selected()

		path = construct_path(store, iter)
		handler.open_file(path)

	# Control

	def on_control_activate(self, item):
		treeview = self.glade.get_widget('treeview')
		selection = treeview.get_selection()
		store, iter = selection.get_selected()
		project, name = construct_project_name(store, iter)

		treepath = store.get_path(iter)
		controller = self._get_controller(store, treepath)
		controller.control(name)

	def on_control_recursive_activate(self, item):
		treeview = self.glade.get_widget('treeview')
		selection = treeview.get_selection()
		store, iter = selection.get_selected()
		project, name = construct_project_name(store, iter)

		treepath = store.get_path(iter)
		controller = self._get_controller(store, treepath)
		controller.control(name, recursive=True)

	def on_release_activate(self, item):
		treeview = self.glade.get_widget('treeview')
		selection = treeview.get_selection()
		store, iter = selection.get_selected()
		project, name = construct_project_name(store, iter)

		treepath = store.get_path(iter)
		controller = self._get_controller(store, treepath)
		controller.release(name)

	def on_compare_activate(self, item):
		treeview = self.glade.get_widget('treeview')
		selection = treeview.get_selection()
		store, iter = selection.get_selected()

		treepath = store.get_path(iter)
		controller = self._get_controller(store, treepath)

		root, path = self._get_selected()
		name = path[len(root)+1:]

		data = controller.get_latest(name)
		environment.compare(data, path)

	# Execute

	def on_selected_build_activate(self, item):
		self._selected_action('build')

	def on_selected_clean_activate(self, item):
		self._selected_action('clean')

	def _selected_action(self, action_name):
		batch = execution.Batch()

		treeview = self.glade.get_widget('treeview')
		store = treeview.get_model()

		for treepath in self.sticky_selections.get_paths():
			iter = store.get_iter(treepath)
			root, name = construct_project_name(store, iter)
			options = self._create_options(root)
			action_func = getattr(options, action_name)
			action_func(batch)

		environment.execute(batch)

	def on_tree_build_activate(self, item):
		name, options = self._get_name_and_options()
		if options:
			self._build(options, None)

	def on_dir_build_activate(self, item):
		name, options = self._get_name_and_options()
		if name and options:
			self._build(options, name)

	def on_tree_clean_activate(self, item):
		name, options = self._get_name_and_options()
		if options:
			self._clean(options, None)

	def on_dir_clean_activate(self, item):
		name, options = self._get_name_and_options()
		if name and options:
			self._clean(options, name)

	def _build(self, options, target):
		batch = execution.Batch()
		options.build(batch, target)
		environment.execute(batch)

	def _clean(self, options, target):
		batch = execution.Batch()
		options.clean(batch, target)
		environment.execute(batch)

	def on_file_run_activate(self, item):
		name, options = self._get_name_and_options()
		if not options:
			return

		batch = execution.Batch()
		options.run(batch, name)

		if not self.last_run_batch:
			item = self.ui.get_widget("/menubar/project/run-last")
			item.set_sensitive(True)

		self.last_run_batch = batch

		environment.execute(batch)

	def on_last_run_activate(self, item):
		if self.last_run_batch:
			self.last_run_batch.rewind()
			environment.execute(self.last_run_batch)

	def _get_name_and_options(self):
		treeview = self.glade.get_widget('treeview')
		selection = treeview.get_selection()
		store, iter = selection.get_selected()

		if iter:
			root, name = construct_project_name(store, iter)
			options = self._create_options(root)

			return name, options
		else:
			return None, None

	# Tree

	def on_treeview_expanded(self, treeview, iter, treepath):
		store = treeview.get_model()
		self._open_directory(store, iter)

	def on_treeview_collapsed(self, treeview, iter, treepath):
		store = treeview.get_model()
		self._close_directory(store, iter)
		store.append(iter, PLACEHOLDER)

	def on_treeview_activated(self, treeview, treepath, column):
		store = treeview.get_model()

		iter = store.get_iter(treepath)
		is_dir = store.get_value(iter, 1)
		path = construct_path(store, iter)

		if is_dir:
			if treeview.row_expanded(treepath):
				treeview.collapse_row(treepath)
			else:
				treeview.expand_row(treepath, False)
		else:
			root_iter = store.get_iter(treepath[:1])
			root_path = construct_path(store, root_iter)

			environment.edit(path, workdir=root_path)

	def on_treeview_press(self, treeview, event):
		if event.button != 3:
			return

		selection = treeview.get_selection()
		store, iter = selection.get_selected()
		if not iter:
			return

		root_path = construct_path(store, iter)

		if not store.iter_parent(iter):
			name = '/tree-popup'
		elif store.iter_children(iter):
			name = '/dir-popup'
		else:
			name = '/file-popup'

		menu = self.ui.get_widget(name)
		menu.popup(None, None, None, event.button, event.time)

	# Destroy

	def on_scroller_destroy(self, scroller):
		self.conf.remove_all()
		self.conf = None

	# View

	def get_label(self):
		return self.label

	def get_widget(self):
		return self.scroller

gobject.type_register(View)

# Extension types

class OptionsExtension (extension.Extension):
	'''Project options.'''

	def __init__(self):
		extension.Extension.__init__(self)

	def get_controller_name(self):
		'''Get the class name of the controller that should be used for managing the
		   contents of this project.  None implies automatic
		   controller selection.'''
		return None

	def build(self, batch, name=None):
		'''Add jobs to BATCH for building target NAME of this project.  Build default
		   target if NAME is not set.'''
		pass

	def clean(self, batch, name=None):
		'''Add jobs to BATCH for cleaning up target NAME of this project.  Clean
		   default target if NAME is not set.'''
		pass

	def run(self, batch, name=None):
		'''Add job to BATCH for running target NAME of this project.  Run default
		   target if NAME is not set.'''
		pass

gobject.type_register(OptionsExtension)

class ControllerExtension (extension.Extension):
	'''Project content management using version control system or such
	   back-end.'''

	def __init__(self):
		extension.Extension.__init__(self)

	def is_controlled(self, name):
		'''Is NAME known to the system?'''
		return False

	def is_modified(self, name):
		'''Has NAME been modified since the last version known to the system?'''
		return False

	def get_latest(self, name):
		'''Get the contents of the last version of NAME that is known to the
		   system.'''
		return None

	def control(self, name, recursive=False):
		'''Register NAME with the system.  If recursive is set and NAME is a
		   directory, its contents should be registered.'''
		pass

	def release(self, name):
		'''Unregister NAME from the system.  If NAME is a directory, its contents
		   should be unregistered.'''
		pass

	def make_file(self, name, control=False):
		'''Create the empty NAME file.  Register it with the system if CONTROL is
		   set.'''
		pass

	def make_directory(self, name, control=False):
		'''Create the NAME directory.  Register it with the system if CONTROL is
		   set.'''
		pass

	def remove(self, name, control=False):
		'''Delete NAME.  If NAME is a directory, its contents should be deleted.
		   Unregister it from the system if CONTROL is set.'''
		pass

	def rename(self, old_name, new_name, control=False):
		'''Rename OLD_NAME as NEW_NAME.  Update the change to the system if CONTROL
		   is set.'''
		pass

	def clone(self, old_name, new_name, control=False):
		'''Copy OLD_NAME to NEW_NAME.  If OLD_NAME is a directory, its contents
		   should be copied.  Register the created files and
		   directories with the system if CONTROL is set.'''
		pass

gobject.type_register(ControllerExtension)
