# Copyright 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import gobject
import gtk

##
## Vertical scroller

class VScroller (gtk.HBox):
	def __init__(self, widget):
		gtk.HBox.__init__(self)

		self.delta = 0.0

		self.adjustment = gtk.Adjustment()
		self.adjustment.connect('changed', self.on_adjustment_changed)

		widget.set_scroll_adjustments(None, self.adjustment)
		widget.connect('scroll-event', self.on_widget_scroll_event)

		scrollbar = gtk.VScrollbar(self.adjustment)
		scrollbar.show()

		self.pack_start(widget, expand=True, fill=True)
		self.pack_end(scrollbar, expand=False, fill=True)

	def on_adjustment_changed(self, adjustment):
		self.delta = adjustment.page_size ** (2.0 / 3.0)

	def on_widget_scroll_event(self, widget, event):
		if event.direction == gtk.gdk.SCROLL_UP:
			direction = -1
		elif event.direction == gtk.gdk.SCROLL_DOWN:
			direction = 1
		else:
			return False

		value = self.adjustment.get_value()

		value += direction * self.delta
		value = max(value, self.adjustment.lower)
		value = min(value, self.adjustment.upper - self.adjustment.page_size)

		self.adjustment.set_value(value)

		return True

gobject.type_register(VScroller)
