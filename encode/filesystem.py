# Copyright 2005, 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import os
import gobject

import project

class Controller (project.ControllerExtension):
	rules = None

	def __init__(self, path):
		project.ControllerExtension.__init__(self)
		self.root = path

	def make_file(self, name, control=False):
		path = self._get_path(name)
		file = open(path, 'w')
		file.close()

	def make_directory(self, name, control=False):
		path = self._get_path(name)
		os.mkdir(path)

	def remove(self, name, control=False):
		path = self._get_path(name)
		if os.path.isdir(path):
			os.rmdir(path)
		else:
			os.remove(path)

	def rename(self, old_name, new_name, control=False):
		old_path = self._get_path(old_name)
		new_path = self._get_path(new_name)
		os.rename(old_path, new_path)

	def clone(self, old_name, new_name, control=False):
		old_path = self._get_path(old_name)
		new_path = self._get_path(new_name)
		project.copy_hierarchy(old_path, new_path)

	def _get_path(self, name):
		return os.path.join(self.root, name)

gobject.type_register(Controller)
