# Copyright 2005, 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

'''Utility for loading and saving settings.  (A convenience layer on top of GConf.)'''

import gobject
import gconf

class Directory (gobject.GObject):
	'''A handle for accessing settings under a particular GConf directory.  When registering a
	   callback, it will be called immediately with the current value of the associated
	   variable.  The correct set/get/add method needs to be used depending on the type of the
	   target variable.'''

	def __init__(self, name):
		'''Initialise a handle to the "/apps/NAME" directory.'''

		gobject.GObject.__init__(self)

		self.root = '/apps/%s' % name

		self.client = gconf.client_get_default()
		self.client.add_dir(self.root, gconf.CLIENT_PRELOAD_ONELEVEL)

		self.notifications = []

	def _make_path(self, name):
		return '%s/%s' % (self.root, name)

	def directory(self, name):
		'''Create a handle to the NAME subdirectory.'''

		root = self._make_path(name)
		return Config(root)

	def remove_all(self):
		'''Remove all callbacks registered via this handle.'''

		for id in self.notifications:
			self.client.notify_remove(id)

		del self.notifications[:]

	def _add(self, name, callback, validate):
		def call(client, id, entry, data):
			value = validate(entry.value)
			if value is not None:
				callback(value)

		key = self._make_path(name)

		id = self.client.notify_add(key, call)
		self.notifications.append(id)

		gvalue = self.client.get(key)
		value = validate(gvalue)
		if value is not None:
			callback(value)

	def _get(self, name, validate, default):
		key = self._make_path(name)

		gvalue = self.client.get(key)
		value = validate(gvalue)

		if value is not None:
			return value
		else:
			return default

	def add_string(self, name, callback):
		'''Register CALLBACK to be called when the value of the NAME variable changes.'''

		self._add(name, callback, _validate_string)

	def add_strings(self, name, callback):
		'''Register CALLBACK to be called when the value of the NAME variable changes.'''

		self._add(name, callback, _validate_string_list)

	def add_int(self, name, callback):
		'''Register CALLBACK to be called when the value of the NAME variable changes.'''

		self._add(name, callback, _validate_int)

	def add_ints(self, name, callback):
		'''Register CALLBACK to be called when the value of the NAME variable changes.'''

		self._add(name, callback, _validate_int_list)

	def add_bool(self, name, callback):
		'''Register CALLBACK to be called when the value of the NAME variable changes.'''

		self._add(name, callback, _validate_bool)

	def add_bools(self, name, callback):
		'''Register CALLBACK to be called when the value of the NAME variable changes.'''

		self._add(name, callback, _validate_bool_list)

	def get_string(self, name, default = None):
		'''Get the current value of the NAME variable.'''

		return self._get(name, _validate_string, default)

	def get_strings(self, name, default = None):
		'''Get the current value of the NAME variable.'''

		return self._get(name, _validate_string_list, default)

	def get_int(self, name, default = None):
		'''Get the current value of the NAME variable.'''

		return self._get(name, _validate_int, default)

	def get_ints(self, name, default = None):
		'''Get the current value of the NAME variable.'''

		return self._get(name, _validate_int_list, default)

	def get_bool(self, name, default = None):
		'''Get the current value of the NAME variable.'''

		return self._get(name, _validate_bool, default)

	def get_bools(self, name, default = None):
		'''Get the current value of the NAME variable.'''

		return self._get(name, _validate_bool_list, default)

	def set_string(self, name, value):
		'''Change the value of the NAME variable.'''

		key = self._make_path(name)
		self.client.set_string(key, value)

	def set_strings(self, name, values):
		'''Change the value of the NAME variable.'''

		key = self._make_path(name)
		self.client.set_list(key, gconf.VALUE_STRING, values)

	def set_int(self, name, value):
		'''Change the value of the NAME variable.'''

		key = self._make_path(name)
		self.client.set_int(key, value)

	def set_ints(self, name, values):
		'''Change the value of the NAME variable.'''

		key = self._make_path(name)
		self.client.set_list(key, gconf.VALUE_INT, values)

	def set_bool(self, name, value):
		'''Change the value of the NAME variable.'''

		key = self._make_path(name)
		self.client.set_bool(key, value)

	def set_bools(self, name, values):
		'''Change the value of the NAME variable.'''

		key = self._make_path(name)
		self.client.set_list(key, gconf.VALUE_BOOL, values)

	def unset(self, name):
		'''Change the value of the NAME variable to its default.'''

		key = self._make_path(name)
		self.client.unset(key)

gobject.type_register(Directory)
gobject.signal_new('changed', Directory, gobject.SIGNAL_DETAILED, gobject.TYPE_NONE, [gobject.GObject])

def _validate_string(value):
	if value and value.type == gconf.VALUE_STRING:
		return value.get_string()

def _validate_string_list(value):
	if value and value.type == gconf.VALUE_LIST and value.get_list_type() == gconf.VALUE_STRING:
		return [item.get_string() for item in value.get_list()]

def _validate_int(value):
	if value and value.type == gconf.VALUE_INT:
		return value.get_int()

def _validate_int_list(value):
	if value and value.type == gconf.VALUE_LIST and value.get_list_type() == gconf.VALUE_INT:
		return [item.get_int() for item in value.get_list()]

def _validate_bool(value):
	if value is not None and value.type == gconf.VALUE_BOOL:
		return value.get_bool()

def _validate_bool_list(value):
	if value and value.type == gconf.VALUE_LIST and value.get_list_type() == gconf.VALUE_BOOL:
		return [item.get_bool() for item in value.get_list()]
