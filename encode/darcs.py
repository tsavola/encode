# Copyright 2005, 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import os
import re
import filecmp
import gobject

import filesystem
import execution

class Controller (filesystem.Controller):
	rules = ('_darcs',)

	def __init__(self, path):
		filesystem.Controller.__init__(self, path)
		self.execute = execution.Utility(error_type=DarcsError)

	def is_controlled(self, name):
		escaped = escape(os.path.join('.', name))

		re_add = re.compile('^add(file|dir) %s$' % escaped)
		re_remove = re.compile('^rm(file|dir) %s$' % escaped)
		re_move_in = re.compile('^move .* %s$' % escaped)
		re_move_out = re.compile('^move %s ' % escaped)

		pristine = self._get_pristine_path(name)
		control = os.path.exists(pristine)

		pending = os.path.join(self.root, '_darcs', 'patches', 'pending')
		if os.path.exists(pending):
			file = open(pending)

			for line in file:
				line = line.rstrip('\n')

				if control:
					if re_remove.match(line) or re_move_out.match(line):
						control = False
				else:
					if re_add.match(line) or re_move_in.match(line):
						control = True

			file.close()

		return control

	def is_modified(self, name):
		pristine = self._get_pristine_path(name)
		if os.path.exists(pristine):
			if os.path.isdir(pristine):
				return False
			else:
				work = self._get_path(name)
				return not filecmp.cmp(work, pristine)
		else:
			return True

	def get_latest(self, name):
		pristine = self._get_pristine_path(name)
		if os.path.exists(pristine):
			file = open(pristine)
			data = file.read()
			file.close()
			return data
		else:
			return None

	def control(self, name, recursive=False):
		if recursive:
			self._call(['add', '--recursive', name])
		else:
			self._call(['add', name])

	def release(self, name):
		names = self._find_controlled(name)
		self._call(['remove'] + names)

	def make_file(self, name, control=True):
		filesystem.Controller.make_file(self, name)
		if control:
			self._call(['add', name])

	def make_directory(self, name, control=True):
		filesystem.Controller.make_directory(self, name)
		if control:
			self._call(['add', name])

	def remove(self, name, control=True):
		filesystem.Controller.remove(self, name)
		if control:
			self._call(['remove', name])

	def rename(self, old_name, new_name, control=True):
		if control:
			self._call(['mv', old_name, new_name])
		else:
			filesystem.Controller.rename(self, old_name, new_name)

	def clone(self, old_name, new_name, control=True):
		filesystem.Controller.clone(self, old_name, new_name)
		if control:
			self._call(['add', '--recursive', new_name])

	def _get_pristine_path(self, name):
		pristinedir = os.path.join(self.root, '_darcs', 'pristine')
		if not os.path.exists(pristinedir):
			pristinedir = os.path.join(self.root, '_darcs', 'current')

		return os.path.join(pristinedir, name)

	def _find_controlled(self, name):
		contents = [name]

		path = self._get_path(name)
		if os.path.isdir(path):
			for filename in os.listdir(path):
				if filename == '.' or filename == '..':
					continue

				child_name = os.path.join(name, filename)
				if self.is_controlled(child_name):
					child_contents = self._find_controlled(child_name)
					contents = child_contents + contents

		return contents

	def _call(self, params):
		args = ['darcs'] + params
		job = execution.Job(args, workdir=self.root)

		self.execute.without_output(job)

gobject.type_register(Controller)

class DarcsError (execution.UtilityError):
	def __init__(self, message):
		execution.UtilityError.__init__(self, message)

def escape(path):
	for c in '\\.':
		path = path.replace(c, '\\' + c)
	return path
