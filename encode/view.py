# Copyright 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import sys
import os
import gobject
import gtk

import extension

class ViewExtension (extension.Extension):
	'''GUI component.'''

	__gproperties__ = {
		'relocatable': (gobject.TYPE_BOOLEAN,
		                'Is view relocatable?',
		                'The view widget can be unmapped without problems',
		                True,
		                gobject.PARAM_READABLE),
	}

	def __init__(self):
		extension.Extension.__init__(self)

	def do_get_property(self, property):
		if property.name == 'relocatable':
			return self.is_relocatable()
		else:
			raise AttributeError, 'Unknown property: %s' % property.name

	def is_relocatable(self):
		'''Is this view draggable-and-droppable?'''
		return True

	def get_widget(self):
		'''Return the gtk.Widget which is the root of the widget hierarchy that
		   should be attached to the environment window.  This widget
		   receives a "destroy" signal when the view is detached from
		   the environment.  Environment doesn't touch the widget's
		   settings; it's up to the view to set the "visible"
		   property.'''
		return None

	def cleanup(self):
		'''This method will be called before the view is detached from the
		   environment or before the environment is shut down.'''
		pass

gobject.type_register(ViewExtension)

class Embedder (ViewExtension):
	'''Base for XEmbedded views.'''

	def __init__(self):
		ViewExtension.__init__(self)

		self.socket = gtk.Socket()
		self.socket.set_property('can-focus', True)
		self.socket.connect_after('map', self.on_socket_map)
		self.socket.show()

		self.xid = None

	def on_socket_map(self, socket):
		if self.xid is None:
			self.xid = socket.get_id()

			try:
				self.client_start(self.xid)
			except:
				socket.destroy()
				raise

			self.socket.connect_after('destroy', self.on_socket_destroy)

	def on_socket_destroy(self, socket):
		self.client_destroy()

	def get_widget(self):
		return self.socket

	def is_relocatable(self):
		return False

	def client_start(self, xid):
		'''Start the XEmbed client.  XID is the parent window ID.'''
		pass

	def client_destroy(self):
		'''Stop the XEmbed client.'''
		pass

gobject.type_register(Embedder)
