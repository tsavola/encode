# Copyright 2005, 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import os
import signal
import pwd
import gobject
import gtk
import gtk.glade
import vte
from gettext import gettext

import environment
import view
import config
import settings

datadir = None

def init_extension(d):
	global datadir
	datadir = d

##
## Launcher

class Launcher (environment.LauncherExtension):
	def __init__(self):
		environment.LauncherExtension.__init__(self)

		label = gettext('Terminal emulator')
		self.action = gtk.Action(None, label, None, None)
		self.action.set_visible(True)

	def get_action(self):
		return self.action

	def create(self):
		return View()

gobject.type_register(Launcher)

##
## View

drag_targets = (
	('TEXT', 0, 0),
	('STRING', 0, 1),
	('text/uri-list', 0, 10),
	('_NETSCAPE_URL', 0, 11),
)

class View (view.ViewExtension):
	def __init__(self):
		view.ViewExtension.__init__(self)

		self.terminal = vte.Terminal()
		self.terminal.set_property('can-focus', True)
		self.terminal.connect('child-exited', self.on_terminal_child_exited)
		self.terminal.show()

		self.terminal.drag_dest_set(gtk.DEST_DEFAULT_ALL, drag_targets, gtk.gdk.ACTION_COPY)
		self.terminal.connect('drag-data-received', self.on_terminal_received)

		adjustment = self.terminal.get_adjustment()
		scrollbar = gtk.VScrollbar(adjustment)
		scrollbar.show()

		self.box = gtk.HBox()
		self.box.pack_start(self.terminal, expand=True, fill=True)
		self.box.pack_start(scrollbar, expand=False, fill=True)
		self.box.show()

		text = gettext('Terminal')
		self.label = gtk.Label(text)
		self.label.show()

		conf = config.Directory('encode/terminal')
		font    = conf.get_string('font')
		lines   = conf.get_int   ('scrollback_lines')
		chars   = conf.get_string('word_chars')
		command = conf.get_string('command')

		if font:
			self.terminal.set_font_from_string(font)
		if lines and lines > 0:
			self.terminal.set_scrollback_lines(lines)
		if chars:
			self.terminal.set_word_chars(chars)

		if not command:
			uid = os.getuid()
			entry = pwd.getpwuid(uid)
			command = entry[6]

		self.pid = self.terminal.fork_command(command)

	def on_terminal_received(self, terminal, context, x, y, selection, target, timestamp):
		if target < 10:
			text = selection.data
		else:
			prefix = 'file://'
			hostname = 'localhost'

			text = ''
			for uri in selection.data.split():
				if uri.startswith(prefix):
					path = uri[len(prefix):]
					if path.startswith(hostname):
						path = source_path[len(hostname):]
				else:
					path = uri

				text += path
				text += ' '

		self.terminal.feed_child(text)
		self.terminal.grab_focus()

		context.finish(True, False, timestamp)

	def on_terminal_child_exited(self, terminal):
		self.pid = None
		self.box.destroy()

	def cleanup(self):
		if self.pid is not None:
			os.kill(self.pid, signal.SIGHUP)
			self.pid = None

	def get_label(self):
		return self.label

	def get_widget(self):
		return self.box

gobject.type_register(View)

##
## Settings

if 'set' not in __builtins__:
	import sets
	set = sets.Set

class Settings (settings.SettingsExtension):
	def __init__(self):
		settings.SettingsExtension.__init__(self)

		path = os.path.join(datadir, 'encode', 'terminal-settings.glade')
		self.glade = gtk.glade.XML(path)
		self.glade.signal_autoconnect(self)

		self.config = config.Directory('encode/terminal')

		self.dirty = set((
			'font',
			'lines',
		))
		self.revert()

	def get_name(self):
		return gettext('Terminal')

	def get_widget(self):
		return self.glade.get_widget('table')

	def apply(self):
		if 'font' in self.dirty:
			fontbutton = self.glade.get_widget('font_fontbutton')
			font = fontbutton.get_font_name()

			self.config.set_string('font', font)

		if 'lines' in self.dirty:
			spinbutton = self.glade.get_widget('lines_spinbutton')
			lines = spinbutton.get_value_as_int()

			self.config.set_int('scrollback_lines', lines)

		self.dirty.clear()

	def revert(self):
		if 'font' in self.dirty:
			font = self.config.get_string('font')

			fontbutton = self.glade.get_widget('font_fontbutton')
			fontbutton.set_font_name(font)

		if 'lines' in self.dirty:
			lines = self.config.get_int('scrollback_lines')

			spinbutton = self.glade.get_widget('lines_spinbutton')
			spinbutton.set_value(lines)

		self.dirty.clear()

	def on_font_set(self, fontbutton):
		self.dirty.add('font')
		self.modified()

	def on_lines_changed(self, spinbutton):
		self.dirty.add('lines')
		self.modified()

gobject.type_register(Settings)
