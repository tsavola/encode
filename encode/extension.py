# Copyright 2005, 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import sys
import weakref
import gobject

def load(path, name, datadir):
	'''Add PATH to module load path and import all classes matching NAME .  Call
	   the init_extension function with DATADIR if the imported modules
	   have one.  Extension classes are expected to register themselves
	   as GObjects.'''

	origpath = sys.path

	if path:
		sys.path = [path] + sys.path

	try:
		if name.endswith('.*'):
			pkgname = name[:-2]
			pkg = _import(pkgname)

			names = ['%s.%s' % (pkgname, i) for i in pkg.__all__]
		else:
			names = [name]

		for name in names:
			try:
				mod = _import(name)

				if 'init_extension' in dir(mod):
					mod.init_extension(datadir)

			except Exception, e:
				if path:
					pretty = '%s:%s' % (path, name)
				else:
					pretty = name

				print >>sys.stderr, 'Failed to load module %s (%s)' % (pretty, e)

	finally:
		sys.path = origpath

def _import(name):
	list = name.split('.')
	return __import__(name, globals(), locals(), list[:-1])

refs_by_class = {}

def get_children(parent):
	'''Get a list of classes that are derived from PARENT (must be a GObject).'''

	immediate = [i.pytype for i in gobject.type_children(parent)]

	rest = []
	for i in immediate:
		rest += get_children(i)

	return immediate + rest

def get_singleton_children(parent):
	return [get_singleton(i) for i in get_children(parent)]

def get_singleton(cls):
	if '__instance' not in dir(cls):
		cls.__instance = cls()
	return cls.__instance

def get_class(name):
	'''Get a class by its NAME (must be a GObject).'''

	gname = name.replace('.', '+')
	gtype = gobject.type_from_name(gname)
	return gtype.pytype

class Extension (gobject.GObject):
	'''Base for extension tag classes.'''

	def __init__(self):
		gobject.GObject.__init__(self)

	def destroy(self):
		'''Emits the "destroy" signal.'''
		self.emit('destroy')

gobject.type_register(Extension)
gobject.signal_new('destroy', Extension, 0, gobject.TYPE_NONE, [])
