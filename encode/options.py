# Copyright 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

import os
import gobject

import project
import execution

class GuessOptions (project.OptionsExtension):
	rules = None

	def __init__(self, path, match):
		project.OptionsExtension.__init__(self)
		self.root = path

	def _get_path(self, name):
		return os.path.join(self.root, name)

	def build(self, batch, name=None):
		if os.path.exists(os.path.join(self.root, 'SConstruct')):
			args = ['scons']
			if name:
				args.append(name)
		else:
			args = ['make']
			if name:
				if os.path.isdir(os.path.join(self.root, name)):
					args += ['-C', name]
				else:
					dir, file = os.path.split(name)
					if dir:
						args += ['-C', dir]
					args.append(file)

		job = execution.Job(args, workdir=self.root)
		batch.add(job)

	def clean(self, batch, name=None):
		if os.path.exists(os.path.join(self.root, 'SConstruct')):
			args = ['scons', '-c']
			if name:
				args.append(name)
		else:
			args = ['make', 'clean']
			if name:
				args += ['-C', name]

		job = execution.Job(args, workdir=self.root)
		batch.add(job)

	def run(self, batch, name=None):
		if name:
			dir, file = os.path.split(name)
			if not dir:
				name = os.path.join('.', file)

			job = execution.Job([name], workdir=self.root)
			batch.add(job)

gobject.type_register(GuessOptions)

class PythonOptions (GuessOptions):
	rules = (
		'EncodeOptions.py',
		'./$(DIRNAME).encode.py',
	)

	def __init__(self, path, match):
		GuessOptions.__init__(self, path, match)

		if match:
			execfile(match, self.__dict__)

gobject.type_register(PythonOptions)
