#!/usr/bin/env python

import gettext
gettext.textdomain('encode')

import sys
sys.path = ['.'] + sys.path

import os, encode
for name in os.listdir('encode'):
	if name.endswith('.py') and \
	   name != '__init__.py' and \
	   name[:-3] not in encode.__all__:
		print >>sys.stderr, 'Warning:', name, 'is missing from __init__.py'

import encode.environment
encode.environment.main('.', 'Development')
